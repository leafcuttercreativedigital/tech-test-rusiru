<?php

// include get_template_directory() . '/ChromePhp.php';

#-----------------------------------------------------------------#
# Theme setup
#-----------------------------------------------------------------#

function leafcutter_theme_enqueue_styles()
{
    wp_enqueue_style('leafcutter-styling', get_template_directory_uri() . '/style.css');
}
add_action('wp_enqueue_scripts', 'leafcutter_theme_enqueue_styles');

function my_force_login() {
    global $post;
    
    if (!is_single()) {
        return;
    } else {
        if(!is_user_logged_in()){
            auth_redirect();
        }
    }
} 

function leafcutter_theme_enqueue_scripts()
{
    global $post;
    global $user;
 
        wp_register_script('test-scripts', get_stylesheet_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', true);
        wp_localize_script('test-scripts', 'script_vars', array(
                'postID' => $post->ID
            )
        );
        wp_enqueue_script('test-scripts');
  
}

