<?php
/*
Plugin Name: Raisely Widgets for WordPress
Version: 0.1
Plugin URI: https://leafcutter.com.au/open-source/raisely-widgets
Description: A simple plugin with a few Raisely widgets you can use on your site
Author: James Hornitzky
Author URI: https://leafcutter.com.au/

WC requires at least: 2.6
WC tested up to: 3.4.5
*/

//enqueue required scripts and styles
function raisely_widgets_enqueue_scripts() {   
    wp_enqueue_script( 'raisely-widgets-scripts', plugin_dir_url( __FILE__ ) . 'raisely-widgets.js' );
    wp_enqueue_style('raisely-widgets-styles', plugin_dir_url( __FILE__ ) . 'raisely-widgets.css');
}
add_action('wp_enqueue_scripts', 'raisely_widgets_enqueue_scripts');

//[raisely] shortcode
function raisely_widgets_shortcode( $atts ){

    //generate the container
    $uuid = 'rw-'.uniqid();
    $output = '<div id="'.$uuid.'" class="raisely-widget"></div>';

    //append script based on logic type
    if ($atts['widget'] == 'leaderboard') {
        $a = shortcode_atts( array(
            'widget' => '',
            'type' => '',
            'campaign_uuid' => '',
            'public_url' => '',
            'limit'=>5
        ), $atts );

        $output .= 
            '<script type="text/javascript"> 
                document.addEventListener("DOMContentLoaded", function(){
                    raiselyHelper.widgetLeaderboard("'.$uuid.'","'.$a['campaign_uuid'].'","'.$a['public_url'].'","'.$a['type'].'",'.$a['limit'].'); 
                });
            </script>';

    } else if ($atts['widget'] == 'find-a-friend') {
        $a = shortcode_atts( array(
            'widget' => '',
            'campaign_uuid' => '',
            'public_url' => ''
        ), $atts );

        $output .= 
           '<script type="text/javascript"> 
                document.addEventListener("DOMContentLoaded", function(){
                    raiselyHelper.widgetFindFriends("'.$uuid.'","'.$a['campaign_uuid'].'","'.$a['public_url'].'");
                });
            </script>';
    }
    
	return $output;
}
add_shortcode( 'raisely', 'raisely_widgets_shortcode' );
