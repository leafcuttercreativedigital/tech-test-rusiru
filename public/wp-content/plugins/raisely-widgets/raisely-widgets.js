var raiselyHelper = {
    //Raisely API URL
    apiBaseUrl: 'https://api.raisely.com/v3', 

    //GET Raisely Helper
    getRaisely:function(urlString, callback) {
        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === this.DONE) {
                callback(JSON.parse(this.responseText));
            }
        });

        xhr.open("GET", this.apiBaseUrl + '/' + urlString);

        xhr.send();
    },

    //Raisely widget functions

    //Generate Leaderboard Widget
    widgetLeaderboard: function (domId, campaignUuid, publicUrl, type, limit) {
        this.getRaisely('campaigns/' + campaignUuid + '/profiles/?type=' + type + '&sort=total&limit='+limit, function (response) {
            var output = '';

            output += '<ul class="leaderboard">';
            response.data.forEach(function(profile) {
                output += 
                '<li class="profile">\
                    <a href="'+ publicUrl + '/' + profile.path + '">\
                        <div class="profile-image" style="background-image:url('+ profile.photoUrl + ')"></div>\
                        <div class="name">'+ profile.name + '</div>\
                        <div class="total">$'+ Math.floor(profile.total / 100) +'</div>\
                    </a>\
                </li>'; 
            });
            output += '</ul>';

            document.getElementById(domId).innerHTML = output; 
        });
    },

    //Generate Find Friends Widget
    widgetFindFriends: function (domId, campaignUuid, publicUrl) {
        function myFunction() {
            myVar = setTimeout(function () { alert("Hello"); }, 3000);
        }

        function myStopFunction() {
            clearTimeout(myVar);
        }

        var output = '';

        output += 
            '<div class="find-friends-search">\
                <input type="text" onkeyup="raiselyHelper.actionFindFriends(\''+domId+'\',\''+campaignUuid+'\',\''+publicUrl+'\')"/>\
            </div>\
            <div class="find-friends-results">\
            </div>';

        document.getElementById(domId).innerHTML = output; 
    },

    //find friends action on the dropdown
    actionFindFriends: function (domId, campaignUuid, publicUrl) {
        var searchTerms = document.querySelector('#' + domId + ' .find-friends-search input').value;

        clearTimeout(this.actionTimeout);

        var self = this;

        this.actionTimeout = setTimeout(function () { 
            self.getRaisely('campaigns/' + campaignUuid + '/profiles/?q=' + searchTerms + '&limit=5', function (response) {
                console.log('findFriends:');
                console.log(response);
                var output = '';

                output += '<ul class="friends">';
                response.data.forEach(function (profile) {
                    output +=
                        '<li class="profile">\
                            <a href="'+ publicUrl + '/' + profile.path + '">\
                                <div class="profile-image" style="background-image:url('+ profile.photoUrl + ')"></div>\
                                <div class="name">'+ profile.name + '</div>\
                                <div class="total">$'+ Math.floor(profile.total / 100) + '</div>\
                            </a>\
                        </li>';
                });
                output += '</ul>';

                document.querySelector('#' + domId + ' .find-friends-results').innerHTML = output; 
            });
        }, 500);
    }
}