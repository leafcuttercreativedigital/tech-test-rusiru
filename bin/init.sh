#!/bin/bash -eux

DB_NAME=${WP_DB_NAME:-wp}
DB_USER=${WP_DB_USER:-root}
DB_PASS=${WP_DB_PASS:-root}
DB_HOST=${WP_DB_HOST:-127.0.0.1}
DB_DUMP=${WP_DB_DUMP:-bin/wp_db.sql}

n=0
until [ $n -ge 5 ]
do
	(mysql -u${DB_USER} -p${DB_PASS} -h${DB_HOST} --protocol=TCP -e "CREATE DATABASE IF NOT EXISTS ${DB_NAME}" \
    || mysql -u${DB_USER} -p${DB_PASS} -h${DB_HOST} --protocol=TCP ${DB_NAME} < ${DB_DUMP} \
	|| mysql -u${DB_USER} -p${DB_PASS} -h${DB_HOST} --protocol=TCP ${DB_NAME} -e "show tables from ${DB_NAME}") \
	&& break

	n=$[$n+1]
	sleep 15
done

wp config create \
	--allow-root \
	--dbname=${DB_NAME} \
	--dbuser=${DB_USER} \
	--dbpass=${DB_PASS} \
	--dbhost=${DB_HOST} \
	--force

cat << EOF > .htaccess
<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews
    </IfModule>

    RewriteEngine On

    # Prevent access to sensitive files
    RedirectMatch 404 \.(yml|md|sh|sql|ini|lock)$

    # Prevent access to hidden files
    RedirectMatch 404 /\..*$

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>
EOF
