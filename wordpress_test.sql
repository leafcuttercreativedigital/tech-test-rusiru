-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2019 at 01:10 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wordpress_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_cf7_vdata`
--

CREATE TABLE `wp_cf7_vdata` (
  `id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_cf7_vdata_entry`
--

CREATE TABLE `wp_cf7_vdata_entry` (
  `id` int(11) NOT NULL,
  `cf7_id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-10-02 08:24:37', '2019-10-02 08:24:37', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 46, 'ActionScheduler', '', '', '', '2019-10-03 05:19:12', '2019-10-03 05:19:12', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(3, 46, 'ActionScheduler', '', '', '', '2019-10-03 05:19:31', '2019-10-03 05:19:31', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(4, 46, 'ActionScheduler', '', '', '', '2019-10-03 05:19:33', '2019-10-03 05:19:33', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(5, 65, 'ruzi', 'rusiru@leafcutter.com.au', '', '127.0.0.1', '2019-10-03 08:17:00', '2019-10-03 08:17:00', '4', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://test.local.com/', 'yes'),
(2, 'home', 'http://test.local.com/', 'yes'),
(3, 'blogname', 'SALIENT', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'rusiru@leafcutter.com.au', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:233:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:53:\"project-type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?project-type=$matches[1]&feed=$matches[2]\";s:48:\"project-type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?project-type=$matches[1]&feed=$matches[2]\";s:29:\"project-type/([^/]+)/embed/?$\";s:45:\"index.php?project-type=$matches[1]&embed=true\";s:41:\"project-type/([^/]+)/page/?([0-9]{1,})/?$\";s:52:\"index.php?project-type=$matches[1]&paged=$matches[2]\";s:23:\"project-type/([^/]+)/?$\";s:34:\"index.php?project-type=$matches[1]\";s:59:\"project-attributes/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:57:\"index.php?project-attributes=$matches[1]&feed=$matches[2]\";s:54:\"project-attributes/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:57:\"index.php?project-attributes=$matches[1]&feed=$matches[2]\";s:35:\"project-attributes/([^/]+)/embed/?$\";s:51:\"index.php?project-attributes=$matches[1]&embed=true\";s:47:\"project-attributes/([^/]+)/page/?([0-9]{1,})/?$\";s:58:\"index.php?project-attributes=$matches[1]&paged=$matches[2]\";s:29:\"project-attributes/([^/]+)/?$\";s:40:\"index.php?project-attributes=$matches[1]\";s:57:\"slider-locations/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?slider-locations=$matches[1]&feed=$matches[2]\";s:52:\"slider-locations/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?slider-locations=$matches[1]&feed=$matches[2]\";s:33:\"slider-locations/([^/]+)/embed/?$\";s:49:\"index.php?slider-locations=$matches[1]&embed=true\";s:45:\"slider-locations/([^/]+)/page/?([0-9]{1,})/?$\";s:56:\"index.php?slider-locations=$matches[1]&paged=$matches[2]\";s:27:\"slider-locations/([^/]+)/?$\";s:38:\"index.php?slider-locations=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:37:\"portfolio/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"portfolio/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"portfolio/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"portfolio/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"portfolio/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"portfolio/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"portfolio/([^/]+)/embed/?$\";s:42:\"index.php?portfolio=$matches[1]&embed=true\";s:30:\"portfolio/([^/]+)/trackback/?$\";s:36:\"index.php?portfolio=$matches[1]&tb=1\";s:38:\"portfolio/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?portfolio=$matches[1]&paged=$matches[2]\";s:45:\"portfolio/([^/]+)/comment-page-([0-9]{1,})/?$\";s:49:\"index.php?portfolio=$matches[1]&cpage=$matches[2]\";s:35:\"portfolio/([^/]+)/wc-api(/(.*))?/?$\";s:50:\"index.php?portfolio=$matches[1]&wc-api=$matches[3]\";s:41:\"portfolio/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:52:\"portfolio/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:34:\"portfolio/([^/]+)(?:/([0-9]+))?/?$\";s:48:\"index.php?portfolio=$matches[1]&page=$matches[2]\";s:26:\"portfolio/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:36:\"portfolio/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:56:\"portfolio/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"portfolio/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"portfolio/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:32:\"portfolio/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:39:\"home_slider/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:49:\"home_slider/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:69:\"home_slider/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"home_slider/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"home_slider/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:45:\"home_slider/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:28:\"home_slider/([^/]+)/embed/?$\";s:44:\"index.php?home_slider=$matches[1]&embed=true\";s:32:\"home_slider/([^/]+)/trackback/?$\";s:38:\"index.php?home_slider=$matches[1]&tb=1\";s:40:\"home_slider/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?home_slider=$matches[1]&paged=$matches[2]\";s:47:\"home_slider/([^/]+)/comment-page-([0-9]{1,})/?$\";s:51:\"index.php?home_slider=$matches[1]&cpage=$matches[2]\";s:37:\"home_slider/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?home_slider=$matches[1]&wc-api=$matches[3]\";s:43:\"home_slider/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:54:\"home_slider/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:36:\"home_slider/([^/]+)(?:/([0-9]+))?/?$\";s:50:\"index.php?home_slider=$matches[1]&page=$matches[2]\";s:28:\"home_slider/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:38:\"home_slider/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:58:\"home_slider/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"home_slider/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"home_slider/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:34:\"home_slider/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:41:\"nectar_slider/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"nectar_slider/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"nectar_slider/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"nectar_slider/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"nectar_slider/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"nectar_slider/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"nectar_slider/([^/]+)/embed/?$\";s:46:\"index.php?nectar_slider=$matches[1]&embed=true\";s:34:\"nectar_slider/([^/]+)/trackback/?$\";s:40:\"index.php?nectar_slider=$matches[1]&tb=1\";s:42:\"nectar_slider/([^/]+)/page/?([0-9]{1,})/?$\";s:53:\"index.php?nectar_slider=$matches[1]&paged=$matches[2]\";s:49:\"nectar_slider/([^/]+)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?nectar_slider=$matches[1]&cpage=$matches[2]\";s:39:\"nectar_slider/([^/]+)/wc-api(/(.*))?/?$\";s:54:\"index.php?nectar_slider=$matches[1]&wc-api=$matches[3]\";s:45:\"nectar_slider/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:56:\"nectar_slider/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:38:\"nectar_slider/([^/]+)(?:/([0-9]+))?/?$\";s:52:\"index.php?nectar_slider=$matches[1]&page=$matches[2]\";s:30:\"nectar_slider/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"nectar_slider/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"nectar_slider/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"nectar_slider/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"nectar_slider/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"nectar_slider/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=7&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:62:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:62:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:6:{i:0;s:35:\"advanced-cf7-db/advanced-cf7-db.php\";i:1;s:33:\"classic-editor/classic-editor.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:33:\"duplicate-post/duplicate-post.php\";i:4;s:35:\"js_composer_salient/js_composer.php\";i:5;s:27:\"woocommerce/woocommerce.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:3:{i:0;s:84:\"C:\\xampp\\htdocs\\wordpress-template\\public/wp-content/themes/leafcutter/functions.php\";i:1;s:80:\"C:\\xampp\\htdocs\\wordpress-template\\public/wp-content/themes/leafcutter/style.css\";i:3;s:0:\"\";}', 'no'),
(40, 'template', 'salient', 'yes'),
(41, 'stylesheet', 'leafcutter', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:33:\"classic-editor/classic-editor.php\";a:2:{i:0;s:14:\"Classic_Editor\";i:1;s:9:\"uninstall\";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '7', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'wp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:119:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:10:\"copy_posts\";b:1;s:18:\"cf7_db_form_view40\";b:1;s:19:\"cf7_db_form_edit_40\";b:1;s:17:\"cf7_db_form_view6\";b:1;s:18:\"cf7_db_form_edit_6\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'en_AU', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:3:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;i:4;a:3:{s:5:\"title\";s:19:\"FRESH FROM THE BLOG\";s:6:\"number\";i:3;s:9:\"show_date\";b:0;}}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:10:{s:19:\"wp_inactive_widgets\";a:0:{}s:12:\"blog-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"page-sidebar\";a:0:{}s:19:\"woocommerce-sidebar\";a:0:{}s:20:\"nectar-extra-sidebar\";a:0:{}s:13:\"footer-area-1\";a:1:{i:0;s:14:\"recent-posts-4\";}s:13:\"footer-area-2\";a:1:{i:0;s:10:\"nav_menu-3\";}s:13:\"footer-area-3\";a:1:{i:0;s:13:\"custom_html-3\";}s:21:\"footer-area-copyright\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:14:{i:1570183225;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:0:{}s:8:\"interval\";i:60;}}}i:1570183497;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1570184680;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1570184892;a:1:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1570184902;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1570185738;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1570195692;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1570206492;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1570220680;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1570233600;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1570263879;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1570263915;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1572912000;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:2:{s:12:\"_multiwidget\";i:1;i:3;a:2:{s:5:\"title\";s:10:\"Quick Menu\";s:8:\"nav_menu\";i:18;}}', 'yes'),
(112, 'widget_custom_html', 'a:2:{s:12:\"_multiwidget\";i:1;i:3;a:2:{s:5:\"title\";s:10:\"NEWSLETTER\";s:7:\"content\";s:52:\"[contact-form-7 id=\"40\" title=\"Contact form 1_copy\"]\";}}', 'yes'),
(114, 'recovery_keys', 'a:0:{}', 'yes'),
(126, '_site_transient_timeout_browser_40d2af28a4c309bbb824dc957af59b11', '1570609500', 'no'),
(127, '_site_transient_browser_40d2af28a4c309bbb824dc957af59b11', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"77.0.3865.90\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(129, '_site_transient_timeout_php_check_03b757470e94c3bf37ab9d7408701106', '1570609501', 'no'),
(130, '_site_transient_php_check_03b757470e94c3bf37ab9d7408701106', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(131, 'can_compress_scripts', '1', 'no'),
(145, 'theme_mods_twentynineteen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1570004720;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(146, 'current_theme', 'Leafcutter Template', 'yes'),
(147, 'theme_mods_salient', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1570013136;s:4:\"data\";a:9:{s:19:\"wp_inactive_widgets\";a:0:{}s:12:\"blog-sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:12:\"page-sidebar\";a:0:{}s:19:\"woocommerce-sidebar\";a:0:{}s:20:\"nectar-extra-sidebar\";a:0:{}s:13:\"footer-area-1\";a:0:{}s:13:\"footer-area-2\";a:0:{}s:13:\"footer-area-3\";a:0:{}s:13:\"footer-area-4\";a:0:{}}}}', 'yes'),
(148, 'theme_switched', '', 'yes'),
(149, 'widget_nectar_popular_posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(150, 'shop_catalog_image_size', 'a:3:{s:5:\"width\";s:3:\"375\";s:6:\"height\";s:3:\"400\";s:4:\"crop\";i:1;}', 'yes'),
(151, 'shop_single_image_size', 'a:3:{s:5:\"width\";s:3:\"600\";s:6:\"height\";s:3:\"630\";s:4:\"crop\";i:1;}', 'yes'),
(152, 'shop_thumbnail_image_size', 'a:3:{s:5:\"width\";s:3:\"130\";s:6:\"height\";s:3:\"130\";s:4:\"crop\";i:1;}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(153, 'salient_redux', 'a:453:{s:8:\"last_tab\";s:1:\"1\";s:10:\"theme-skin\";s:8:\"original\";s:14:\"button-styling\";s:7:\"default\";s:16:\"overall-bg-color\";s:7:\"#ffffff\";s:18:\"overall-font-color\";s:0:\"\";s:11:\"body-border\";s:1:\"0\";s:17:\"body-border-color\";s:7:\"#ffffff\";s:16:\"body-border-size\";s:0:\"\";s:11:\"back-to-top\";s:1:\"1\";s:18:\"back-to-top-mobile\";s:1:\"0\";s:18:\"one-page-scrolling\";s:1:\"1\";s:10:\"responsive\";s:1:\"1\";s:14:\"ext_responsive\";s:1:\"1\";s:19:\"max_container_width\";s:4:\"1425\";s:15:\"lightbox_script\";s:8:\"fancybox\";s:16:\"default-lightbox\";s:1:\"0\";s:23:\"disable-mobile-parallax\";s:1:\"0\";s:24:\"disable-mobile-video-bgs\";s:1:\"0\";s:23:\"column_animation_easing\";s:12:\"easeOutCubic\";s:23:\"column_animation_timing\";s:3:\"750\";s:20:\"external-dynamic-css\";s:1:\"0\";s:16:\"google-analytics\";s:0:\"\";s:19:\"google-maps-api-key\";s:0:\"\";s:10:\"custom-css\";s:0:\"\";s:11:\"disable_tgm\";s:1:\"0\";s:22:\"disable_home_slider_pt\";s:1:\"0\";s:24:\"disable_nectar_slider_pt\";s:1:\"0\";s:12:\"accent-color\";s:7:\"#27ccc0\";s:13:\"extra-color-1\";s:7:\"#ff1053\";s:13:\"extra-color-2\";s:7:\"#2AC4EA\";s:13:\"extra-color-3\";s:7:\"#333333\";s:20:\"extra-color-gradient\";a:2:{s:4:\"from\";s:7:\"#3452ff\";s:2:\"to\";s:7:\"#ff1053\";}s:22:\"extra-color-gradient-2\";a:2:{s:4:\"from\";s:7:\"#2AC4EA\";s:2:\"to\";s:7:\"#32d6ff\";}s:12:\"boxed_layout\";s:1:\"0\";s:16:\"background-color\";s:7:\"#f1f1f1\";s:16:\"background_image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:17:\"background-repeat\";s:0:\"\";s:19:\"background-position\";s:0:\"\";s:21:\"background-attachment\";s:0:\"\";s:16:\"background-cover\";s:1:\"0\";s:19:\"extended-theme-font\";s:1:\"0\";s:22:\"navigation_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:31:\"navigation_dropdown_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:24:\"page_heading_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:33:\"page_heading_subtitle_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:26:\"off_canvas_nav_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:34:\"off_canvas_nav_subtext_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:16:\"body_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:14:\"h1_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:14:\"h2_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:14:\"h3_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:14:\"h4_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:14:\"h5_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:14:\"h6_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:13:\"i_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:17:\"label_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:33:\"nectar_slider_heading_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:31:\"home_slider_caption_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:23:\"testimonial_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:28:\"sidebar_footer_h_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:29:\"portfolio_filters_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:29:\"portfolio_caption_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:25:\"team_member_h_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:26:\"nectar_dropcap_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:41:\"nectar_sidebar_footer_headers_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:41:\"nectar_woo_shop_product_title_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:45:\"nectar_woo_shop_product_secondary_font_family\";a:10:{s:11:\"font-family\";s:0:\"\";s:12:\"font-options\";s:0:\"\";s:6:\"google\";s:1:\"1\";s:11:\"font-weight\";s:0:\"\";s:10:\"font-style\";s:0:\"\";s:7:\"subsets\";s:0:\"\";s:14:\"text-transform\";s:0:\"\";s:9:\"font-size\";s:0:\"\";s:11:\"line-height\";s:0:\"\";s:14:\"letter-spacing\";s:0:\"\";}s:33:\"use-responsive-heading-typography\";s:0:\"\";s:26:\"h1-small-desktop-font-size\";s:2:\"75\";s:19:\"h1-tablet-font-size\";s:2:\"70\";s:18:\"h1-phone-font-size\";s:2:\"65\";s:26:\"h2-small-desktop-font-size\";s:2:\"85\";s:19:\"h2-tablet-font-size\";s:2:\"80\";s:18:\"h2-phone-font-size\";s:2:\"70\";s:26:\"h3-small-desktop-font-size\";s:2:\"85\";s:19:\"h3-tablet-font-size\";s:2:\"80\";s:18:\"h3-phone-font-size\";s:2:\"70\";s:26:\"h4-small-desktop-font-size\";s:3:\"100\";s:19:\"h4-tablet-font-size\";s:2:\"90\";s:18:\"h4-phone-font-size\";s:2:\"90\";s:26:\"h5-small-desktop-font-size\";s:3:\"100\";s:19:\"h5-tablet-font-size\";s:3:\"100\";s:18:\"h5-phone-font-size\";s:3:\"100\";s:26:\"h6-small-desktop-font-size\";s:3:\"100\";s:19:\"h6-tablet-font-size\";s:3:\"100\";s:18:\"h6-phone-font-size\";s:3:\"100\";s:28:\"body-small-desktop-font-size\";s:3:\"100\";s:21:\"body-tablet-font-size\";s:3:\"100\";s:20:\"body-phone-font-size\";s:3:\"100\";s:8:\"use-logo\";s:1:\"1\";s:4:\"logo\";a:5:{s:3:\"url\";s:65:\"http://test.local.com/wp-content/uploads/2019/10/salient-logo.png\";s:2:\"id\";s:2:\"19\";s:6:\"height\";s:2:\"30\";s:5:\"width\";s:3:\"251\";s:9:\"thumbnail\";s:72:\"http://test.local.com/wp-content/uploads/2019/10/salient-logo-150x30.png\";}s:11:\"retina-logo\";a:5:{s:3:\"url\";s:65:\"http://test.local.com/wp-content/uploads/2019/10/salient-logo.png\";s:2:\"id\";s:2:\"19\";s:6:\"height\";s:2:\"30\";s:5:\"width\";s:3:\"251\";s:9:\"thumbnail\";s:72:\"http://test.local.com/wp-content/uploads/2019/10/salient-logo-150x30.png\";}s:11:\"logo-height\";s:2:\"30\";s:18:\"mobile-logo-height\";s:0:\"\";s:11:\"mobile-logo\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:14:\"header-padding\";s:0:\"\";s:19:\"header-remove-fixed\";s:1:\"0\";s:19:\"header-mobile-fixed\";s:1:\"0\";s:29:\"header-menu-mobile-breakpoint\";s:4:\"1000\";s:17:\"header-box-shadow\";s:5:\"small\";s:24:\"header-menu-item-spacing\";s:1:\"8\";s:17:\"header-bg-opacity\";s:3:\"100\";s:12:\"header-color\";s:5:\"light\";s:23:\"header-background-color\";s:7:\"#ffffff\";s:17:\"header-font-color\";s:7:\"#888888\";s:23:\"header-font-hover-color\";s:7:\"#27CCC0\";s:32:\"header-dropdown-background-color\";s:7:\"#1F1F1F\";s:38:\"header-dropdown-background-hover-color\";s:7:\"#313233\";s:26:\"header-dropdown-font-color\";s:7:\"#CCCCCC\";s:32:\"header-dropdown-font-hover-color\";s:7:\"#27CCC0\";s:34:\"header-dropdown-heading-font-color\";s:7:\"#ffffff\";s:40:\"header-dropdown-heading-font-hover-color\";s:7:\"#ffffff\";s:22:\"header-separator-color\";s:7:\"#eeeeee\";s:33:\"secondary-header-background-color\";s:7:\"#F8F8F8\";s:27:\"secondary-header-font-color\";s:7:\"#666666\";s:33:\"secondary-header-font-hover-color\";s:7:\"#222222\";s:45:\"header-slide-out-widget-area-background-color\";s:7:\"#27ccc0\";s:47:\"header-slide-out-widget-area-background-color-2\";s:0:\"\";s:41:\"header-slide-out-widget-area-header-color\";s:7:\"#ffffff\";s:34:\"header-slide-out-widget-area-color\";s:7:\"#eefbfa\";s:40:\"header-slide-out-widget-area-hover-color\";s:7:\"#ffffff\";s:43:\"header-slide-out-widget-area-close-bg-color\";s:7:\"#ff1053\";s:45:\"header-slide-out-widget-area-close-icon-color\";s:7:\"#ffffff\";s:13:\"header_format\";s:7:\"default\";s:16:\"header-fullwidth\";s:1:\"0\";s:21:\"header-disable-search\";s:1:\"0\";s:26:\"header-disable-ajax-search\";s:1:\"1\";s:21:\"header-account-button\";s:1:\"0\";s:25:\"header-account-button-url\";s:0:\"\";s:13:\"header_layout\";s:8:\"standard\";s:21:\"secondary-header-text\";s:0:\"\";s:21:\"secondary-header-link\";s:0:\"\";s:23:\"enable_social_in_header\";s:1:\"0\";s:24:\"use-facebook-icon-header\";s:0:\"\";s:23:\"use-twitter-icon-header\";s:0:\"\";s:27:\"use-google-plus-icon-header\";s:0:\"\";s:21:\"use-vimeo-icon-header\";s:0:\"\";s:24:\"use-dribbble-icon-header\";s:0:\"\";s:25:\"use-pinterest-icon-header\";s:0:\"\";s:23:\"use-youtube-icon-header\";s:0:\"\";s:22:\"use-tumblr-icon-header\";s:0:\"\";s:24:\"use-linkedin-icon-header\";s:0:\"\";s:19:\"use-rss-icon-header\";s:0:\"\";s:23:\"use-behance-icon-header\";s:0:\"\";s:25:\"use-instagram-icon-header\";s:0:\"\";s:22:\"use-flickr-icon-header\";s:0:\"\";s:23:\"use-spotify-icon-header\";s:0:\"\";s:22:\"use-github-icon-header\";s:0:\"\";s:29:\"use-stackexchange-icon-header\";s:0:\"\";s:26:\"use-soundcloud-icon-header\";s:0:\"\";s:18:\"use-vk-icon-header\";s:0:\"\";s:20:\"use-vine-icon-header\";s:0:\"\";s:21:\"use-houzz-icon-header\";s:0:\"\";s:20:\"use-yelp-icon-header\";s:0:\"\";s:24:\"use-mixcloud-icon-header\";s:0:\"\";s:24:\"use-snapchat-icon-header\";s:0:\"\";s:24:\"use-bandcamp-icon-header\";s:0:\"\";s:27:\"use-tripadvisor-icon-header\";s:0:\"\";s:24:\"use-telegram-icon-header\";s:0:\"\";s:21:\"use-slack-icon-header\";s:0:\"\";s:22:\"use-medium-icon-header\";s:0:\"\";s:21:\"use-email-icon-header\";s:0:\"\";s:21:\"use-phone-icon-header\";s:0:\"\";s:18:\"transparent-header\";s:1:\"0\";s:20:\"header-starting-logo\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:27:\"header-starting-retina-logo\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:25:\"header-starting-logo-dark\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:32:\"header-starting-retina-logo-dark\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:21:\"header-starting-color\";s:7:\"#ffffff\";s:29:\"header-transparent-dark-color\";s:7:\"#000000\";s:28:\"header-permanent-transparent\";s:1:\"0\";s:24:\"header-inherit-row-color\";s:1:\"0\";s:20:\"header-remove-border\";s:1:\"0\";s:32:\"transparent-header-shadow-helper\";s:1:\"0\";s:19:\"header-hover-effect\";s:18:\"animated_underline\";s:24:\"header-hide-until-needed\";s:0:\"\";s:23:\"header-resize-on-scroll\";s:1:\"1\";s:34:\"header-resize-on-scroll-shrink-num\";s:0:\"\";s:25:\"condense-header-on-scroll\";s:0:\"\";s:21:\"header-dropdown-style\";s:7:\"classic\";s:23:\"header-dropdown-opacity\";s:3:\"100\";s:22:\"header-dropdown-arrows\";s:7:\"inherit\";s:21:\"header-megamenu-width\";s:9:\"contained\";s:34:\"header-megamenu-remove-transparent\";s:1:\"0\";s:28:\"header-slide-out-widget-area\";s:1:\"0\";s:34:\"header-slide-out-widget-area-style\";s:20:\"slide-out-from-right\";s:46:\"header-slide-out-widget-area-dropdown-behavior\";s:7:\"default\";s:35:\"header-slide-out-widget-area-social\";s:1:\"0\";s:40:\"header-slide-out-widget-area-bottom-text\";s:0:\"\";s:44:\"header-slide-out-widget-area-overlay-opacity\";s:4:\"dark\";s:46:\"header-slide-out-widget-area-top-nav-in-mobile\";s:1:\"0\";s:23:\"enable-main-footer-area\";s:1:\"1\";s:14:\"footer_columns\";s:1:\"3\";s:19:\"footer-custom-color\";s:1:\"1\";s:23:\"footer-background-color\";s:7:\"#313233\";s:17:\"footer-font-color\";s:7:\"#CCCCCC\";s:27:\"footer-secondary-font-color\";s:7:\"#777777\";s:33:\"footer-copyright-background-color\";s:7:\"#1F1F1F\";s:27:\"footer-copyright-font-color\";s:7:\"#777777\";s:33:\"footer-copyright-icon-hover-color\";s:7:\"#ffffff\";s:21:\"footer-copyright-line\";s:1:\"1\";s:17:\"footer-full-width\";s:1:\"0\";s:13:\"footer-reveal\";s:1:\"0\";s:20:\"footer-reveal-shadow\";s:4:\"none\";s:23:\"footer-copyright-layout\";s:8:\"centered\";s:29:\"disable-copyright-footer-area\";s:0:\"\";s:21:\"footer-copyright-text\";s:21:\"© 2019 Movie Rental.\";s:22:\"disable-auto-copyright\";s:1:\"1\";s:23:\"footer-background-image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:31:\"footer-background-image-overlay\";s:3:\"0.8\";s:17:\"use-facebook-icon\";s:0:\"\";s:16:\"use-twitter-icon\";s:0:\"\";s:20:\"use-google-plus-icon\";s:0:\"\";s:14:\"use-vimeo-icon\";s:0:\"\";s:17:\"use-dribbble-icon\";s:0:\"\";s:18:\"use-pinterest-icon\";s:0:\"\";s:16:\"use-youtube-icon\";s:0:\"\";s:15:\"use-tumblr-icon\";s:0:\"\";s:17:\"use-linkedin-icon\";s:0:\"\";s:12:\"use-rss-icon\";s:0:\"\";s:16:\"use-behance-icon\";s:0:\"\";s:18:\"use-instagram-icon\";s:0:\"\";s:15:\"use-flickr-icon\";s:0:\"\";s:16:\"use-spotify-icon\";s:0:\"\";s:15:\"use-github-icon\";s:0:\"\";s:22:\"use-stackexchange-icon\";s:0:\"\";s:19:\"use-soundcloud-icon\";s:0:\"\";s:11:\"use-vk-icon\";s:0:\"\";s:13:\"use-vine-icon\";s:0:\"\";s:14:\"use-houzz-icon\";s:0:\"\";s:13:\"use-yelp-icon\";s:0:\"\";s:17:\"use-snapchat-icon\";s:0:\"\";s:17:\"use-mixcloud-icon\";s:0:\"\";s:17:\"use-bandcamp-icon\";s:0:\"\";s:20:\"use-tripadvisor-icon\";s:0:\"\";s:17:\"use-telegram-icon\";s:0:\"\";s:14:\"use-slack-icon\";s:0:\"\";s:15:\"use-medium-icon\";s:0:\"\";s:17:\"ajax-page-loading\";s:1:\"1\";s:17:\"transition-method\";s:8:\"standard\";s:32:\"disable-transition-fade-on-click\";s:1:\"0\";s:28:\"disable-transition-on-mobile\";s:1:\"1\";s:17:\"transition-effect\";s:8:\"standard\";s:12:\"loading-icon\";s:8:\"material\";s:19:\"loading-icon-colors\";a:2:{s:4:\"from\";s:7:\"#3452ff\";s:2:\"to\";s:7:\"#3452ff\";}s:13:\"loading-image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:23:\"loading-image-animation\";s:4:\"none\";s:19:\"transition-bg-color\";s:0:\"\";s:21:\"transition-bg-color-2\";s:0:\"\";s:17:\"header-auto-title\";s:1:\"0\";s:24:\"header-animate-in-effect\";s:4:\"none\";s:23:\"header-down-arrow-style\";s:7:\"default\";s:10:\"form-style\";s:7:\"default\";s:17:\"form-fancy-select\";s:1:\"0\";s:21:\"form-submit-btn-style\";s:7:\"regular\";s:8:\"cta-text\";s:0:\"\";s:7:\"cta-btn\";s:0:\"\";s:12:\"cta-btn-link\";s:0:\"\";s:20:\"cta-background-color\";s:7:\"#ECEBE9\";s:14:\"cta-text-color\";s:7:\"#4B4F52\";s:13:\"cta-btn-color\";s:12:\"accent-color\";s:21:\"main_portfolio_layout\";s:1:\"3\";s:28:\"main_portfolio_project_style\";s:1:\"1\";s:27:\"main_portfolio_item_spacing\";s:7:\"default\";s:21:\"portfolio_use_masonry\";s:1:\"0\";s:29:\"portfolio_masonry_grid_sizing\";s:7:\"default\";s:24:\"portfolio_inline_filters\";s:1:\"0\";s:20:\"portfolio_single_nav\";s:13:\"after_project\";s:27:\"portfolio_loading_animation\";s:19:\"fade_in_from_bottom\";s:24:\"portfolio_sidebar_follow\";s:1:\"0\";s:16:\"portfolio_social\";s:1:\"1\";s:22:\"portfolio_social_style\";s:18:\"fixed_bottom_right\";s:26:\"portfolio-facebook-sharing\";s:1:\"1\";s:25:\"portfolio-twitter-sharing\";s:1:\"1\";s:29:\"portfolio-google-plus-sharing\";s:1:\"1\";s:27:\"portfolio-pinterest-sharing\";s:1:\"1\";s:26:\"portfolio-linkedin-sharing\";s:1:\"1\";s:14:\"portfolio_date\";s:1:\"1\";s:20:\"portfolio_pagination\";s:1:\"0\";s:25:\"portfolio_pagination_type\";s:7:\"default\";s:26:\"portfolio_extra_pagination\";s:1:\"0\";s:27:\"portfolio_pagination_number\";s:0:\"\";s:25:\"portfolio_remove_comments\";s:1:\"0\";s:22:\"portfolio_rewrite_slug\";s:0:\"\";s:14:\"carousel-title\";s:0:\"\";s:13:\"carousel-link\";s:0:\"\";s:23:\"portfolio-sortable-text\";s:0:\"\";s:19:\"main-portfolio-link\";s:0:\"\";s:34:\"portfolio_same_category_single_nav\";s:1:\"0\";s:9:\"blog_type\";s:22:\"masonry-blog-fullwidth\";s:18:\"blog_standard_type\";s:17:\"featured_img_left\";s:17:\"blog_masonry_type\";s:25:\"auto_meta_overlaid_spaced\";s:25:\"blog_auto_masonry_spacing\";s:3:\"8px\";s:22:\"blog_loading_animation\";s:19:\"fade_in_from_bottom\";s:16:\"blog_header_type\";s:15:\"default_minimal\";s:17:\"blog_hide_sidebar\";s:1:\"1\";s:14:\"blog_enable_ss\";s:1:\"0\";s:24:\"blog_hide_featured_image\";s:1:\"0\";s:21:\"blog_archive_bg_image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:39:\"blog_post_header_inherit_featured_image\";s:1:\"0\";s:10:\"author_bio\";s:1:\"1\";s:17:\"blog_auto_excerpt\";s:1:\"0\";s:19:\"blog_excerpt_length\";s:0:\"\";s:19:\"blog_next_post_link\";s:1:\"1\";s:25:\"blog_next_post_link_style\";s:19:\"fullwidth_next_prev\";s:18:\"blog_related_posts\";s:1:\"0\";s:24:\"blog_related_posts_style\";s:8:\"material\";s:29:\"blog_related_posts_title_text\";s:13:\"related_posts\";s:11:\"blog_social\";s:1:\"1\";s:17:\"blog_social_style\";s:18:\"fixed_bottom_right\";s:21:\"blog-facebook-sharing\";s:1:\"1\";s:20:\"blog-twitter-sharing\";s:1:\"1\";s:24:\"blog-google-plus-sharing\";s:1:\"1\";s:22:\"blog-pinterest-sharing\";s:1:\"1\";s:21:\"blog-linkedin-sharing\";s:1:\"1\";s:12:\"display_tags\";s:1:\"0\";s:17:\"display_full_date\";s:1:\"0\";s:20:\"blog_pagination_type\";s:7:\"default\";s:16:\"extra_pagination\";s:1:\"0\";s:18:\"recent-posts-title\";s:0:\"\";s:17:\"recent-posts-link\";s:0:\"\";s:23:\"blog_remove_single_date\";s:0:\"\";s:25:\"blog_remove_single_author\";s:0:\"\";s:33:\"blog_remove_single_comment_number\";s:0:\"\";s:30:\"blog_remove_single_nectar_love\";s:0:\"\";s:21:\"blog_remove_post_date\";s:0:\"\";s:23:\"blog_remove_post_author\";s:0:\"\";s:31:\"blog_remove_post_comment_number\";s:0:\"\";s:28:\"blog_remove_post_nectar_love\";s:0:\"\";s:11:\"enable-cart\";s:1:\"0\";s:15:\"ajax-cart-style\";s:8:\"dropdown\";s:16:\"main_shop_layout\";s:10:\"no-sidebar\";s:21:\"single_product_layout\";s:10:\"no-sidebar\";s:13:\"product_style\";s:7:\"classic\";s:20:\"product_desktop_cols\";s:7:\"default\";s:26:\"product_desktop_small_cols\";s:7:\"default\";s:19:\"product_tablet_cols\";s:7:\"default\";s:18:\"product_phone_cols\";s:7:\"default\";s:18:\"product_quick_view\";s:0:\"\";s:16:\"product_bg_color\";s:7:\"#ffffff\";s:24:\"product_minimal_bg_color\";s:7:\"#ffffff\";s:24:\"product_archive_bg_color\";s:7:\"#f6f6f6\";s:23:\"product_hover_alt_image\";s:1:\"0\";s:27:\"single_product_gallery_type\";s:10:\"ios_slider\";s:20:\"product_tab_position\";s:9:\"fullwidth\";s:21:\"woo-products-per-page\";s:0:\"\";s:20:\"woo_hide_product_sku\";s:1:\"0\";s:10:\"woo_social\";s:1:\"1\";s:16:\"woo_social_style\";s:18:\"fixed_bottom_right\";s:20:\"woo-facebook-sharing\";s:1:\"1\";s:19:\"woo-twitter-sharing\";s:1:\"1\";s:23:\"woo-google-plus-sharing\";s:1:\"1\";s:21:\"woo-pinterest-sharing\";s:1:\"1\";s:20:\"woo-linkedin-sharing\";s:1:\"0\";s:21:\"search-results-layout\";s:7:\"default\";s:30:\"search-results-header-bg-color\";s:0:\"\";s:32:\"search-results-header-font-color\";s:0:\"\";s:30:\"search-results-header-bg-image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:17:\"page-404-bg-color\";s:0:\"\";s:19:\"page-404-font-color\";s:0:\"\";s:17:\"page-404-bg-image\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:31:\"page-404-bg-image-overlay-color\";s:0:\"\";s:20:\"page-404-home-button\";s:1:\"1\";s:24:\"sharing_btn_accent_color\";s:1:\"1\";s:12:\"facebook-url\";s:0:\"\";s:11:\"twitter-url\";s:0:\"\";s:15:\"google-plus-url\";s:0:\"\";s:9:\"vimeo-url\";s:0:\"\";s:12:\"dribbble-url\";s:0:\"\";s:13:\"pinterest-url\";s:0:\"\";s:11:\"youtube-url\";s:0:\"\";s:10:\"tumblr-url\";s:0:\"\";s:12:\"linkedin-url\";s:0:\"\";s:7:\"rss-url\";s:0:\"\";s:11:\"behance-url\";s:0:\"\";s:10:\"flickr-url\";s:0:\"\";s:11:\"spotify-url\";s:0:\"\";s:13:\"instagram-url\";s:0:\"\";s:10:\"github-url\";s:0:\"\";s:17:\"stackexchange-url\";s:0:\"\";s:14:\"soundcloud-url\";s:0:\"\";s:6:\"vk-url\";s:0:\"\";s:8:\"vine-url\";s:0:\"\";s:9:\"houzz-url\";s:0:\"\";s:8:\"yelp-url\";s:0:\"\";s:12:\"snapchat-url\";s:0:\"\";s:12:\"mixcloud-url\";s:0:\"\";s:12:\"bandcamp-url\";s:0:\"\";s:15:\"tripadvisor-url\";s:0:\"\";s:12:\"telegram-url\";s:0:\"\";s:9:\"slack-url\";s:0:\"\";s:10:\"medium-url\";s:0:\"\";s:9:\"email-url\";s:0:\"\";s:9:\"phone-url\";s:0:\"\";s:10:\"zoom-level\";s:0:\"\";s:15:\"enable-map-zoom\";s:1:\"0\";s:10:\"center-lat\";s:0:\"\";s:10:\"center-lng\";s:0:\"\";s:14:\"use-marker-img\";s:1:\"0\";s:10:\"marker-img\";a:5:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";}s:20:\"enable-map-animation\";s:1:\"1\";s:11:\"map-point-1\";s:1:\"0\";s:9:\"latitude1\";s:0:\"\";s:10:\"longitude1\";s:0:\"\";s:9:\"map-info1\";s:0:\"\";s:11:\"map-point-2\";s:1:\"0\";s:9:\"latitude2\";s:0:\"\";s:10:\"longitude2\";s:0:\"\";s:9:\"map-info2\";s:0:\"\";s:11:\"map-point-3\";s:1:\"0\";s:9:\"latitude3\";s:0:\"\";s:10:\"longitude3\";s:0:\"\";s:9:\"map-info3\";s:0:\"\";s:11:\"map-point-4\";s:1:\"0\";s:9:\"latitude4\";s:0:\"\";s:10:\"longitude4\";s:0:\"\";s:9:\"map-info4\";s:0:\"\";s:11:\"map-point-5\";s:1:\"0\";s:9:\"latitude5\";s:0:\"\";s:10:\"longitude5\";s:0:\"\";s:9:\"map-info5\";s:0:\"\";s:11:\"map-point-6\";s:1:\"0\";s:9:\"latitude6\";s:0:\"\";s:10:\"longitude6\";s:0:\"\";s:9:\"map-info6\";s:0:\"\";s:11:\"map-point-7\";s:1:\"0\";s:9:\"latitude7\";s:0:\"\";s:10:\"longitude7\";s:0:\"\";s:9:\"map-info7\";s:0:\"\";s:11:\"map-point-8\";s:1:\"0\";s:9:\"latitude8\";s:0:\"\";s:10:\"longitude8\";s:0:\"\";s:9:\"map-info8\";s:0:\"\";s:11:\"map-point-9\";s:1:\"0\";s:9:\"latitude9\";s:0:\"\";s:10:\"longitude9\";s:0:\"\";s:9:\"map-info9\";s:0:\"\";s:12:\"map-point-10\";s:1:\"0\";s:10:\"latitude10\";s:0:\"\";s:11:\"longitude10\";s:0:\"\";s:10:\"map-info10\";s:0:\"\";s:20:\"add-remove-locations\";s:0:\"\";s:13:\"map-greyscale\";s:1:\"0\";s:9:\"map-color\";s:0:\"\";s:14:\"map-ultra-flat\";s:0:\"\";s:21:\"map-dark-color-scheme\";s:0:\"\";s:24:\"slider-caption-animation\";s:1:\"1\";s:23:\"slider-background-cover\";s:1:\"1\";s:15:\"slider-autoplay\";s:1:\"1\";s:20:\"slider-advance-speed\";s:0:\"\";s:22:\"slider-animation-speed\";s:0:\"\";s:13:\"slider-height\";s:0:\"\";s:15:\"slider-bg-color\";s:7:\"#000000\";}', 'yes'),
(154, 'salient_redux-transients', 'a:2:{s:14:\"changed_values\";a:0:{}s:9:\"last_save\";i:1570084997;}', 'yes'),
(159, 'widget_recent-posts-extra', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(160, 'widget_recent-projects', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(163, '_transient_timeout_plugin_slugs', '1570186105', 'no'),
(164, '_transient_plugin_slugs', 'a:18:{i:0;s:35:\"advanced-cf7-db/advanced-cf7-db.php\";i:1;s:19:\"akismet/akismet.php\";i:2;s:47:\"better-search-replace/better-search-replace.php\";i:3;s:33:\"classic-editor/classic-editor.php\";i:4;s:36:\"contact-form-7/wp-contact-form-7.php\";i:5;s:33:\"duplicate-post/duplicate-post.php\";i:6;s:81:\"duracelltomi-google-tag-manager/duracelltomi-google-tag-manager-for-wordpress.php\";i:7;s:33:\"instant-images/instant-images.php\";i:8;s:35:\"raisely-widgets/raisely-widgets.php\";i:9;s:27:\"redirection/redirection.php\";i:10;s:35:\"js_composer_salient/js_composer.php\";i:11;s:23:\"wp-smushit/wp-smush.php\";i:12;s:33:\"w3-total-cache/w3-total-cache.php\";i:13;s:27:\"woocommerce/woocommerce.php\";i:14;s:23:\"wordfence/wordfence.php\";i:15;s:35:\"wp-sentry-integration/wp-sentry.php\";i:16;s:35:\"wp-sitemap-page/wp-sitemap-page.php\";i:17;s:24:\"wordpress-seo/wp-seo.php\";}', 'no'),
(165, 'recently_activated', 'a:0:{}', 'yes'),
(166, 'vc_version', '5.6', 'yes'),
(167, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.4\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1570010481;s:7:\"version\";s:5:\"5.1.4\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(170, 'woocommerce_store_address', '', 'yes'),
(171, 'woocommerce_store_address_2', '', 'yes'),
(172, 'woocommerce_store_city', '', 'yes'),
(173, 'woocommerce_default_country', 'GB', 'yes'),
(174, 'woocommerce_store_postcode', '', 'yes'),
(175, 'woocommerce_allowed_countries', 'all', 'yes'),
(176, 'woocommerce_all_except_countries', '', 'yes'),
(177, 'woocommerce_specific_allowed_countries', '', 'yes'),
(178, 'woocommerce_ship_to_countries', '', 'yes'),
(179, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(180, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(181, 'woocommerce_calc_taxes', 'no', 'yes'),
(182, 'woocommerce_enable_coupons', 'yes', 'yes'),
(183, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(184, 'woocommerce_currency', 'GBP', 'yes'),
(185, 'woocommerce_currency_pos', 'left', 'yes'),
(186, 'woocommerce_price_thousand_sep', ',', 'yes'),
(187, 'woocommerce_price_decimal_sep', '.', 'yes'),
(188, 'woocommerce_price_num_decimals', '2', 'yes'),
(189, 'woocommerce_shop_page_id', '', 'yes'),
(190, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(191, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(192, 'woocommerce_placeholder_image', '13', 'yes'),
(193, 'woocommerce_weight_unit', 'kg', 'yes'),
(194, 'woocommerce_dimension_unit', 'cm', 'yes'),
(195, 'woocommerce_enable_reviews', 'yes', 'yes'),
(196, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(197, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(198, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(199, 'woocommerce_review_rating_required', 'yes', 'no'),
(200, 'woocommerce_manage_stock', 'yes', 'yes'),
(201, 'woocommerce_hold_stock_minutes', '60', 'no'),
(202, 'woocommerce_notify_low_stock', 'yes', 'no'),
(203, 'woocommerce_notify_no_stock', 'yes', 'no'),
(204, 'woocommerce_stock_email_recipient', 'rusiru@leafcutter.com.au', 'no'),
(205, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(206, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(207, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(208, 'woocommerce_stock_format', '', 'yes'),
(209, 'woocommerce_file_download_method', 'force', 'no'),
(210, 'woocommerce_downloads_require_login', 'no', 'no'),
(211, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(212, 'woocommerce_prices_include_tax', 'no', 'yes'),
(213, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(214, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(215, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(216, 'woocommerce_tax_classes', '', 'yes'),
(217, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(218, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(219, 'woocommerce_price_display_suffix', '', 'yes'),
(220, 'woocommerce_tax_total_display', 'itemized', 'no'),
(221, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(222, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(223, 'woocommerce_ship_to_destination', 'billing', 'no'),
(224, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(225, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(226, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(227, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(228, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(229, 'woocommerce_registration_generate_username', 'yes', 'no'),
(230, 'woocommerce_registration_generate_password', 'yes', 'no'),
(231, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(232, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(233, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(234, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(235, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(236, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(237, 'woocommerce_trash_pending_orders', '', 'no'),
(238, 'woocommerce_trash_failed_orders', '', 'no'),
(239, 'woocommerce_trash_cancelled_orders', '', 'no'),
(240, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(241, 'woocommerce_email_from_name', 'SALIENT', 'no'),
(242, 'woocommerce_email_from_address', 'rusiru@leafcutter.com.au', 'no'),
(243, 'woocommerce_email_header_image', '', 'no'),
(244, 'woocommerce_email_footer_text', '{site_title} &mdash; Built with {WooCommerce}', 'no'),
(245, 'woocommerce_email_base_color', '#96588a', 'no'),
(246, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(247, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(248, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(249, 'woocommerce_cart_page_id', '', 'no'),
(250, 'woocommerce_checkout_page_id', '', 'no'),
(251, 'woocommerce_myaccount_page_id', '', 'no'),
(252, 'woocommerce_terms_page_id', '', 'no'),
(253, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(254, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(255, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(256, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(257, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(258, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(259, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(260, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(261, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(262, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(263, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(264, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(265, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(266, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(267, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(268, 'woocommerce_api_enabled', 'no', 'yes'),
(269, 'woocommerce_allow_tracking', 'no', 'no'),
(270, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(271, 'woocommerce_single_image_width', '600', 'yes'),
(272, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(273, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(274, 'woocommerce_demo_store', 'no', 'no'),
(275, 'woocommerce_admin_notices', 'a:4:{i:0;s:7:\"install\";i:1;s:20:\"no_secure_connection\";i:2;s:8:\"wc_admin\";i:3;s:14:\"template_files\";}', 'yes'),
(282, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(283, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(284, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(285, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(287, 'default_product_cat', '15', 'yes'),
(290, 'woocommerce_version', '3.7.0', 'yes'),
(291, 'woocommerce_db_version', '3.7.0', 'yes'),
(292, '_transient_timeout_external_ip_address_127.0.0.1', '1570616903', 'no'),
(293, '_transient_external_ip_address_127.0.0.1', '123.231.15.217', 'no'),
(294, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(295, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(296, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(297, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(298, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(299, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(300, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(301, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(302, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(303, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(304, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(305, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(306, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(309, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(315, '_transient_timeout_wc_low_stock_count', '1572604122', 'no'),
(316, '_transient_wc_low_stock_count', '0', 'no'),
(317, '_transient_timeout_wc_outofstock_count', '1572604123', 'no'),
(318, '_transient_wc_outofstock_count', '0', 'no'),
(319, '_transient_timeout_wc_report_sales_by_date', '1570187034', 'no'),
(320, '_transient_wc_report_sales_by_date', 'a:16:{s:32:\"9e1538974a4c6beb5eb2a42ca58efceb\";a:0:{}s:32:\"e69bea6f1b1b27dbcb980dc891319c76\";a:0:{}s:32:\"20f06d436044b787882c2b3dcb6033c2\";a:0:{}s:32:\"32b8b226ff9e025209a89a3695978178\";N;s:32:\"dd59117f7144ec6360aa23fedf919052\";a:0:{}s:32:\"60c7d0be395c6a57683e2118c50b3999\";a:0:{}s:32:\"c6a525399ab4a6b8dc9d4459fe5af81a\";a:0:{}s:32:\"00e472a6581a8fb3947a644aeba5ff4a\";a:0:{}s:32:\"3c1d2592d8bab41ae348f11d47ab1dfc\";a:0:{}s:32:\"5b0df05627af518f1da82438536b3c64\";a:0:{}s:32:\"5b88696125f205b040d842e7f626dccb\";a:0:{}s:32:\"654bac942d05d8f227965b4a99fb0e80\";N;s:32:\"f7ba88afb08a0d8206f92c544a8388d1\";a:0:{}s:32:\"6bb4256d1e3ebc2716716a33e8cd4044\";a:0:{}s:32:\"8e49dc73962af195aa147c9c6353df04\";a:0:{}s:32:\"e40e4fd0609944b9212b946cd7d77b57\";a:0:{}}', 'no'),
(321, '_transient_timeout_wc_admin_report', '1570187034', 'no'),
(322, '_transient_wc_admin_report', 'a:2:{s:32:\"3420c6a7225c694bcb5435afc28c0250\";a:0:{}s:32:\"e198c0305e28d61c6b2a7e08a4ee5789\";a:0:{}}', 'no'),
(323, '_transient_timeout_wc_term_counts', '1572672111', 'no'),
(324, '_transient_wc_term_counts', 'a:6:{i:15;s:0:\"\";i:19;s:0:\"\";i:20;s:0:\"\";i:21;s:0:\"\";i:22;s:0:\"\";i:23;s:0:\"\";}', 'no'),
(327, 'slider-locations_children', 'a:0:{}', 'yes'),
(328, '_transient_timeout_external_ip_address_::1', '1570616988', 'no'),
(329, '_transient_external_ip_address_::1', '123.231.15.217', 'no'),
(331, '_transient_product_query-transient-version', '1570174995', 'yes'),
(350, 'theme_mods_leafcutter', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:7:\"top_nav\";i:17;s:13:\"secondary_nav\";i:0;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(351, 'woocommerce_maybe_regenerate_images_hash', 'f295711100eef51cf7195e603d0739ad', 'yes'),
(446, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(449, '_transient_shipping-transient-version', '1570073051', 'yes'),
(450, '_transient_timeout_wc_shipping_method_count_legacy', '1572665051', 'no'),
(451, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1570073051\";s:5:\"value\";i:0;}', 'no'),
(555, '_transient_timeout_wc_product_loop_c416215302c0feeebfe95717525ad56e', '1572672200', 'no'),
(556, '_transient_wc_product_loop_c416215302c0feeebfe95717525ad56e', 'a:2:{s:7:\"version\";s:10:\"1570079952\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:0:{}s:5:\"total\";i:0;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:12;s:12:\"current_page\";i:1;}}', 'no'),
(557, '_transient_timeout_wc_product_loop_56b0a6a27cbc8e36397f075ca669a9d6', '1572671450', 'no'),
(558, '_transient_wc_product_loop_56b0a6a27cbc8e36397f075ca669a9d6', 'a:2:{s:7:\"version\";s:10:\"1570076100\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:0:{}s:5:\"total\";i:0;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:12;s:12:\"current_page\";i:1;}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(570, 'woocommerce_marketplace_suggestions', 'a:2:{s:11:\"suggestions\";a:26:{i:0;a:4:{s:4:\"slug\";s:28:\"product-edit-meta-tab-header\";s:7:\"context\";s:28:\"product-edit-meta-tab-header\";s:5:\"title\";s:22:\"Recommended extensions\";s:13:\"allow-dismiss\";b:0;}i:1;a:6:{s:4:\"slug\";s:39:\"product-edit-meta-tab-footer-browse-all\";s:7:\"context\";s:28:\"product-edit-meta-tab-footer\";s:9:\"link-text\";s:21:\"Browse all extensions\";s:3:\"url\";s:64:\"https://woocommerce.com/product-category/woocommerce-extensions/\";s:8:\"promoted\";s:31:\"category-woocommerce-extensions\";s:13:\"allow-dismiss\";b:0;}i:2;a:9:{s:4:\"slug\";s:46:\"product-edit-mailchimp-woocommerce-memberships\";s:7:\"product\";s:33:\"woocommerce-memberships-mailchimp\";s:14:\"show-if-active\";a:1:{i:0;s:23:\"woocommerce-memberships\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:117:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/mailchimp-for-memberships.svg\";s:5:\"title\";s:25:\"Mailchimp for Memberships\";s:4:\"copy\";s:79:\"Completely automate your email lists by syncing membership changes to Mailchimp\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:67:\"https://woocommerce.com/products/mailchimp-woocommerce-memberships/\";}i:3;a:9:{s:4:\"slug\";s:19:\"product-edit-addons\";s:7:\"product\";s:26:\"woocommerce-product-addons\";s:14:\"show-if-active\";a:2:{i:0;s:25:\"woocommerce-subscriptions\";i:1;s:20:\"woocommerce-bookings\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:107:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/product-add-ons.svg\";s:5:\"title\";s:15:\"Product Add-Ons\";s:4:\"copy\";s:93:\"Offer add-ons like gift wrapping, special messages or other special options for your products\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/product-add-ons/\";}i:4;a:9:{s:4:\"slug\";s:46:\"product-edit-woocommerce-subscriptions-gifting\";s:7:\"product\";s:33:\"woocommerce-subscriptions-gifting\";s:14:\"show-if-active\";a:1:{i:0;s:25:\"woocommerce-subscriptions\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:117:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/gifting-for-subscriptions.svg\";s:5:\"title\";s:25:\"Gifting for Subscriptions\";s:4:\"copy\";s:70:\"Let customers buy subscriptions for others - they\'re the ultimate gift\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:67:\"https://woocommerce.com/products/woocommerce-subscriptions-gifting/\";}i:5;a:9:{s:4:\"slug\";s:42:\"product-edit-teams-woocommerce-memberships\";s:7:\"product\";s:33:\"woocommerce-memberships-for-teams\";s:14:\"show-if-active\";a:1:{i:0;s:23:\"woocommerce-memberships\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:113:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/teams-for-memberships.svg\";s:5:\"title\";s:21:\"Teams for Memberships\";s:4:\"copy\";s:123:\"Adds B2B functionality to WooCommerce Memberships, allowing sites to sell team, group, corporate, or family member accounts\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:63:\"https://woocommerce.com/products/teams-woocommerce-memberships/\";}i:6;a:8:{s:4:\"slug\";s:29:\"product-edit-variation-images\";s:7:\"product\";s:39:\"woocommerce-additional-variation-images\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:119:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/additional-variation-images.svg\";s:5:\"title\";s:27:\"Additional Variation Images\";s:4:\"copy\";s:72:\"Showcase your products in the best light with a image for each variation\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:73:\"https://woocommerce.com/products/woocommerce-additional-variation-images/\";}i:7;a:9:{s:4:\"slug\";s:47:\"product-edit-woocommerce-subscription-downloads\";s:7:\"product\";s:34:\"woocommerce-subscription-downloads\";s:14:\"show-if-active\";a:1:{i:0;s:25:\"woocommerce-subscriptions\";}s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:114:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/subscription-downloads.svg\";s:5:\"title\";s:22:\"Subscription Downloads\";s:4:\"copy\";s:57:\"Give customers special downloads with their subscriptions\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:68:\"https://woocommerce.com/products/woocommerce-subscription-downloads/\";}i:8;a:8:{s:4:\"slug\";s:31:\"product-edit-min-max-quantities\";s:7:\"product\";s:30:\"woocommerce-min-max-quantities\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:110:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/min-max-quantities.svg\";s:5:\"title\";s:18:\"Min/Max Quantities\";s:4:\"copy\";s:81:\"Specify minimum and maximum allowed product quantities for orders to be completed\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:52:\"https://woocommerce.com/products/min-max-quantities/\";}i:9;a:8:{s:4:\"slug\";s:28:\"product-edit-name-your-price\";s:7:\"product\";s:27:\"woocommerce-name-your-price\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:107:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/name-your-price.svg\";s:5:\"title\";s:15:\"Name Your Price\";s:4:\"copy\";s:70:\"Let customers pay what they want - useful for donations, tips and more\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/name-your-price/\";}i:10;a:8:{s:4:\"slug\";s:42:\"product-edit-woocommerce-one-page-checkout\";s:7:\"product\";s:29:\"woocommerce-one-page-checkout\";s:7:\"context\";a:1:{i:0;s:26:\"product-edit-meta-tab-body\";}s:4:\"icon\";s:109:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/one-page-checkout.svg\";s:5:\"title\";s:17:\"One Page Checkout\";s:4:\"copy\";s:92:\"Don\'t make customers click around - let them choose products, checkout & pay all on one page\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:63:\"https://woocommerce.com/products/woocommerce-one-page-checkout/\";}i:11;a:4:{s:4:\"slug\";s:19:\"orders-empty-header\";s:7:\"context\";s:24:\"orders-list-empty-header\";s:5:\"title\";s:20:\"Tools for your store\";s:13:\"allow-dismiss\";b:0;}i:12;a:6:{s:4:\"slug\";s:30:\"orders-empty-footer-browse-all\";s:7:\"context\";s:24:\"orders-list-empty-footer\";s:9:\"link-text\";s:21:\"Browse all extensions\";s:3:\"url\";s:64:\"https://woocommerce.com/product-category/woocommerce-extensions/\";s:8:\"promoted\";s:31:\"category-woocommerce-extensions\";s:13:\"allow-dismiss\";b:0;}i:13;a:8:{s:4:\"slug\";s:19:\"orders-empty-zapier\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:18:\"woocommerce-zapier\";s:4:\"icon\";s:98:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/zapier.svg\";s:5:\"title\";s:6:\"Zapier\";s:4:\"copy\";s:88:\"Save time and increase productivity by connecting your store to more than 1000+ services\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:52:\"https://woocommerce.com/products/woocommerce-zapier/\";}i:14;a:8:{s:4:\"slug\";s:30:\"orders-empty-shipment-tracking\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:29:\"woocommerce-shipment-tracking\";s:4:\"icon\";s:109:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/shipment-tracking.svg\";s:5:\"title\";s:17:\"Shipment Tracking\";s:4:\"copy\";s:86:\"Let customers know when their orders will arrive by adding shipment tracking to emails\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:51:\"https://woocommerce.com/products/shipment-tracking/\";}i:15;a:8:{s:4:\"slug\";s:32:\"orders-empty-table-rate-shipping\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:31:\"woocommerce-table-rate-shipping\";s:4:\"icon\";s:111:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/table-rate-shipping.svg\";s:5:\"title\";s:19:\"Table Rate Shipping\";s:4:\"copy\";s:122:\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:53:\"https://woocommerce.com/products/table-rate-shipping/\";}i:16;a:8:{s:4:\"slug\";s:40:\"orders-empty-shipping-carrier-extensions\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:4:\"icon\";s:119:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/shipping-carrier-extensions.svg\";s:5:\"title\";s:27:\"Shipping Carrier Extensions\";s:4:\"copy\";s:116:\"Show live rates from FedEx, UPS, USPS and more directly on your store - never under or overcharge for shipping again\";s:11:\"button-text\";s:13:\"Find Carriers\";s:8:\"promoted\";s:26:\"category-shipping-carriers\";s:3:\"url\";s:99:\"https://woocommerce.com/product-category/woocommerce-extensions/shipping-methods/shipping-carriers/\";}i:17;a:8:{s:4:\"slug\";s:32:\"orders-empty-google-product-feed\";s:7:\"context\";s:22:\"orders-list-empty-body\";s:7:\"product\";s:25:\"woocommerce-product-feeds\";s:4:\"icon\";s:111:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/google-product-feed.svg\";s:5:\"title\";s:19:\"Google Product Feed\";s:4:\"copy\";s:76:\"Increase sales by letting customers find you when they\'re shopping on Google\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:53:\"https://woocommerce.com/products/google-product-feed/\";}i:18;a:4:{s:4:\"slug\";s:35:\"products-empty-header-product-types\";s:7:\"context\";s:26:\"products-list-empty-header\";s:5:\"title\";s:23:\"Other types of products\";s:13:\"allow-dismiss\";b:0;}i:19;a:6:{s:4:\"slug\";s:32:\"products-empty-footer-browse-all\";s:7:\"context\";s:26:\"products-list-empty-footer\";s:9:\"link-text\";s:21:\"Browse all extensions\";s:3:\"url\";s:64:\"https://woocommerce.com/product-category/woocommerce-extensions/\";s:8:\"promoted\";s:31:\"category-woocommerce-extensions\";s:13:\"allow-dismiss\";b:0;}i:20;a:8:{s:4:\"slug\";s:30:\"products-empty-product-vendors\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:27:\"woocommerce-product-vendors\";s:4:\"icon\";s:107:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/product-vendors.svg\";s:5:\"title\";s:15:\"Product Vendors\";s:4:\"copy\";s:47:\"Turn your store into a multi-vendor marketplace\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/product-vendors/\";}i:21;a:8:{s:4:\"slug\";s:26:\"products-empty-memberships\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:23:\"woocommerce-memberships\";s:4:\"icon\";s:103:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/memberships.svg\";s:5:\"title\";s:11:\"Memberships\";s:4:\"copy\";s:76:\"Give members access to restricted content or products, for a fee or for free\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:57:\"https://woocommerce.com/products/woocommerce-memberships/\";}i:22;a:9:{s:4:\"slug\";s:35:\"products-empty-woocommerce-deposits\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:20:\"woocommerce-deposits\";s:14:\"show-if-active\";a:1:{i:0;s:20:\"woocommerce-bookings\";}s:4:\"icon\";s:100:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/deposits.svg\";s:5:\"title\";s:8:\"Deposits\";s:4:\"copy\";s:75:\"Make it easier for customers to pay by offering a deposit or a payment plan\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:54:\"https://woocommerce.com/products/woocommerce-deposits/\";}i:23;a:8:{s:4:\"slug\";s:40:\"products-empty-woocommerce-subscriptions\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:25:\"woocommerce-subscriptions\";s:4:\"icon\";s:105:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/subscriptions.svg\";s:5:\"title\";s:13:\"Subscriptions\";s:4:\"copy\";s:97:\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:59:\"https://woocommerce.com/products/woocommerce-subscriptions/\";}i:24;a:8:{s:4:\"slug\";s:35:\"products-empty-woocommerce-bookings\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:20:\"woocommerce-bookings\";s:4:\"icon\";s:100:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/bookings.svg\";s:5:\"title\";s:8:\"Bookings\";s:4:\"copy\";s:99:\"Allow customers to book appointments, make reservations or rent equipment without leaving your site\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:54:\"https://woocommerce.com/products/woocommerce-bookings/\";}i:25;a:8:{s:4:\"slug\";s:30:\"products-empty-product-bundles\";s:7:\"context\";s:24:\"products-list-empty-body\";s:7:\"product\";s:27:\"woocommerce-product-bundles\";s:4:\"icon\";s:107:\"https://woocommerce.com/wp-content/plugins/wccom-plugins//marketplace-suggestions/icons/product-bundles.svg\";s:5:\"title\";s:15:\"Product Bundles\";s:4:\"copy\";s:49:\"Offer customizable bundles and assembled products\";s:11:\"button-text\";s:10:\"Learn More\";s:3:\"url\";s:49:\"https://woocommerce.com/products/product-bundles/\";}}s:7:\"updated\";i:1570079972;}', 'no'),
(579, 'product_cat_children', 'a:0:{}', 'yes'),
(582, '_transient_timeout_wc_product_loop_ff042c88b12884dfb188f7395612f818', '1572672210', 'no'),
(583, '_transient_wc_product_loop_ff042c88b12884dfb188f7395612f818', 'a:2:{s:7:\"version\";s:10:\"1570079952\";s:5:\"value\";O:8:\"stdClass\":5:{s:3:\"ids\";a:0:{}s:5:\"total\";i:0;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:12;s:12:\"current_page\";i:1;}}', 'no'),
(594, 'project-type_children', 'a:0:{}', 'yes'),
(623, 'duplicate_post_copytitle', '1', 'yes'),
(624, 'duplicate_post_copydate', '0', 'yes'),
(625, 'duplicate_post_copystatus', '0', 'yes'),
(626, 'duplicate_post_copyslug', '0', 'yes'),
(627, 'duplicate_post_copyexcerpt', '1', 'yes'),
(628, 'duplicate_post_copycontent', '1', 'yes'),
(629, 'duplicate_post_copythumbnail', '1', 'yes'),
(630, 'duplicate_post_copytemplate', '1', 'yes'),
(632, 'duplicate_post_copyformat', '1', 'yes'),
(633, 'duplicate_post_copyauthor', '0', 'yes'),
(635, 'duplicate_post_copypassword', '0', 'yes'),
(636, 'duplicate_post_copyattachments', '0', 'yes'),
(637, 'duplicate_post_copychildren', '0', 'yes'),
(638, 'duplicate_post_copycomments', '0', 'yes'),
(639, 'duplicate_post_copymenuorder', '1', 'yes'),
(641, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(642, 'duplicate_post_blacklist', '', 'yes'),
(643, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes'),
(644, 'duplicate_post_show_row', '1', 'yes'),
(645, 'duplicate_post_show_adminbar', '1', 'yes'),
(647, 'duplicate_post_show_submitbox', '1', 'yes'),
(648, 'duplicate_post_show_bulkactions', '1', 'yes'),
(651, 'duplicate_post_version', '3.2.3', 'yes'),
(652, 'duplicate_post_show_notice', '0', 'no'),
(661, 'project-attributes_children', 'a:0:{}', 'yes'),
(754, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:2;s:3:\"all\";i:2;s:8:\"approved\";s:1:\"2\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(755, '_transient_as_comment_count', 'O:8:\"stdClass\":7:{s:8:\"approved\";s:1:\"2\";s:14:\"total_comments\";i:2;s:3:\"all\";i:2;s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(816, 'vsz_cf7_db_version', '1.7.2', 'yes'),
(860, '_transient_timeout__woocommerce_helper_updates', '1570198465', 'no'),
(861, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1570155265;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(960, '_transient_timeout__woocommerce_helper_subscriptions', '1570179406', 'no'),
(961, '_transient__woocommerce_helper_subscriptions', 'a:0:{}', 'no'),
(962, '_site_transient_timeout_theme_roots', '1570180307', 'no'),
(963, '_site_transient_theme_roots', 'a:2:{s:10:\"leafcutter\";s:7:\"/themes\";s:7:\"salient\";s:7:\"/themes\";}', 'no'),
(965, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:3:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/en_AU/wordpress-5.2.3.zip\";s:6:\"locale\";s:5:\"en_AU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/en_AU/wordpress-5.2.3.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.3\";s:7:\"version\";s:5:\"5.2.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.3-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2.3-partial-2.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.3\";s:7:\"version\";s:5:\"5.2.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:5:\"5.2.2\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/en_AU/wordpress-5.2.3.zip\";s:6:\"locale\";s:5:\"en_AU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/en_AU/wordpress-5.2.3.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.3\";s:7:\"version\";s:5:\"5.2.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1570178517;s:15:\"version_checked\";s:5:\"5.2.2\";s:12:\"translations\";a:0:{}}', 'no'),
(966, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1570178521;s:7:\"checked\";a:2:{s:10:\"leafcutter\";s:5:\"1.0.0\";s:7:\"salient\";s:6:\"10.0.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(967, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1570178526;s:8:\"response\";a:3:{s:27:\"redirection/redirection.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/redirection\";s:4:\"slug\";s:11:\"redirection\";s:6:\"plugin\";s:27:\"redirection/redirection.php\";s:11:\"new_version\";s:5:\"4.4.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/redirection/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/redirection.4.4.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/redirection/assets/icon-256x256.jpg?rev=983639\";s:2:\"1x\";s:63:\"https://ps.w.org/redirection/assets/icon-128x128.jpg?rev=983640\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/redirection/assets/banner-1544x500.jpg?rev=983641\";s:2:\"1x\";s:65:\"https://ps.w.org/redirection/assets/banner-772x250.jpg?rev=983642\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.3\";s:12:\"requires_php\";s:3:\"5.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:33:\"w3-total-cache/w3-total-cache.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/w3-total-cache\";s:4:\"slug\";s:14:\"w3-total-cache\";s:6:\"plugin\";s:33:\"w3-total-cache/w3-total-cache.php\";s:11:\"new_version\";s:6:\"0.10.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/w3-total-cache/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/w3-total-cache.0.10.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/w3-total-cache/assets/icon-256x256.png?rev=1041806\";s:2:\"1x\";s:67:\"https://ps.w.org/w3-total-cache/assets/icon-128x128.png?rev=1041806\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/w3-total-cache/assets/banner-772x250.jpg?rev=1041806\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.3\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"12.2\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.12.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:6:\"tested\";s:5:\"5.2.3\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:2:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"classic-editor\";s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:3:\"1.5\";s:7:\"updated\";s:19:\"2019-02-02 15:00:11\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/classic-editor/1.5/en_AU.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"woocommerce\";s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"3.7.0\";s:7:\"updated\";s:19:\"2019-08-15 11:28:26\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/woocommerce/3.7.0/en_AU.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:13:{s:35:\"advanced-cf7-db/advanced-cf7-db.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/advanced-cf7-db\";s:4:\"slug\";s:15:\"advanced-cf7-db\";s:6:\"plugin\";s:35:\"advanced-cf7-db/advanced-cf7-db.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/advanced-cf7-db/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/advanced-cf7-db.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/advanced-cf7-db/assets/icon-256x256.jpg?rev=1696186\";s:2:\"1x\";s:68:\"https://ps.w.org/advanced-cf7-db/assets/icon-128x128.jpg?rev=1696186\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/advanced-cf7-db/assets/banner-1544x500.jpg?rev=1696186\";s:2:\"1x\";s:70:\"https://ps.w.org/advanced-cf7-db/assets/banner-772x250.jpg?rev=1696186\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:47:\"better-search-replace/better-search-replace.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/better-search-replace\";s:4:\"slug\";s:21:\"better-search-replace\";s:6:\"plugin\";s:47:\"better-search-replace/better-search-replace.php\";s:11:\"new_version\";s:5:\"1.3.3\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/better-search-replace/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/better-search-replace.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:74:\"https://ps.w.org/better-search-replace/assets/icon-256x256.png?rev=1238934\";s:2:\"1x\";s:74:\"https://ps.w.org/better-search-replace/assets/icon-128x128.png?rev=1238934\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:77:\"https://ps.w.org/better-search-replace/assets/banner-1544x500.png?rev=1238934\";s:2:\"1x\";s:76:\"https://ps.w.org/better-search-replace/assets/banner-772x250.png?rev=1238934\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"3.2.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=1612753\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=1612753\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=1612986\";}s:11:\"banners_rtl\";a:0:{}}s:81:\"duracelltomi-google-tag-manager/duracelltomi-google-tag-manager-for-wordpress.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:45:\"w.org/plugins/duracelltomi-google-tag-manager\";s:4:\"slug\";s:31:\"duracelltomi-google-tag-manager\";s:6:\"plugin\";s:81:\"duracelltomi-google-tag-manager/duracelltomi-google-tag-manager-for-wordpress.php\";s:11:\"new_version\";s:6:\"1.10.1\";s:3:\"url\";s:62:\"https://wordpress.org/plugins/duracelltomi-google-tag-manager/\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/plugin/duracelltomi-google-tag-manager.1.10.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:84:\"https://ps.w.org/duracelltomi-google-tag-manager/assets/icon-256x256.png?rev=1708451\";s:2:\"1x\";s:84:\"https://ps.w.org/duracelltomi-google-tag-manager/assets/icon-128x128.png?rev=1708451\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:87:\"https://ps.w.org/duracelltomi-google-tag-manager/assets/banner-1544x500.png?rev=1708451\";s:2:\"1x\";s:86:\"https://ps.w.org/duracelltomi-google-tag-manager/assets/banner-772x250.png?rev=1708451\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"instant-images/instant-images.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/instant-images\";s:4:\"slug\";s:14:\"instant-images\";s:6:\"plugin\";s:33:\"instant-images/instant-images.php\";s:11:\"new_version\";s:5:\"4.1.0\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/instant-images/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/instant-images.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/instant-images/assets/icon-256x256.png?rev=1525939\";s:2:\"1x\";s:67:\"https://ps.w.org/instant-images/assets/icon-128x128.png?rev=1525939\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/instant-images/assets/banner-772x250.jpg?rev=2127210\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"wp-smushit/wp-smush.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/wp-smushit\";s:4:\"slug\";s:10:\"wp-smushit\";s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:11:\"new_version\";s:5:\"3.2.4\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/wp-smushit/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/wp-smushit.3.2.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-256x256.jpg?rev=2132251\";s:2:\"1x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-128x128.jpg?rev=2132250\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/wp-smushit/assets/banner-1544x500.png?rev=1863697\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-smushit/assets/banner-772x250.png?rev=1863697\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.7.0\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.7.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2075035\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2075035\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2075035\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2075035\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"wordfence/wordfence.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:23:\"w.org/plugins/wordfence\";s:4:\"slug\";s:9:\"wordfence\";s:6:\"plugin\";s:23:\"wordfence/wordfence.php\";s:11:\"new_version\";s:5:\"7.4.0\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/wordfence/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/wordfence.7.4.0.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:62:\"https://ps.w.org/wordfence/assets/icon-256x256.png?rev=2070855\";s:2:\"1x\";s:54:\"https://ps.w.org/wordfence/assets/icon.svg?rev=2070865\";s:3:\"svg\";s:54:\"https://ps.w.org/wordfence/assets/icon.svg?rev=2070865\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wordfence/assets/banner-1544x500.jpg?rev=2124102\";s:2:\"1x\";s:64:\"https://ps.w.org/wordfence/assets/banner-772x250.jpg?rev=2124102\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"wp-sentry-integration/wp-sentry.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/wp-sentry-integration\";s:4:\"slug\";s:21:\"wp-sentry-integration\";s:6:\"plugin\";s:35:\"wp-sentry-integration/wp-sentry.php\";s:11:\"new_version\";s:5:\"2.8.0\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/wp-sentry-integration/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/wp-sentry-integration.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:74:\"https://ps.w.org/wp-sentry-integration/assets/icon-256x256.jpg?rev=1772463\";s:2:\"1x\";s:74:\"https://ps.w.org/wp-sentry-integration/assets/icon-128x128.jpg?rev=1772463\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:76:\"https://ps.w.org/wp-sentry-integration/assets/banner-772x250.png?rev=1772463\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"wp-sitemap-page/wp-sitemap-page.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/wp-sitemap-page\";s:4:\"slug\";s:15:\"wp-sitemap-page\";s:6:\"plugin\";s:35:\"wp-sitemap-page/wp-sitemap-page.php\";s:11:\"new_version\";s:5:\"1.6.2\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/wp-sitemap-page/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/wp-sitemap-page.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wp-sitemap-page/assets/icon-256x256.png?rev=981368\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-sitemap-page/assets/icon-128x128.png?rev=981368\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/wp-sitemap-page/assets/banner-772x250.png?rev=630898\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(4, 2, '_edit_lock', '1570010682:1'),
(5, 6, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]'),
(6, 6, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:24:\"SALIENT \"[your-subject]\"\";s:6:\"sender\";s:21:\"rusiru@test.local.com\";s:9:\"recipient\";s:24:\"rusiru@leafcutter.com.au\";s:4:\"body\";s:166:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis email was sent from a contact form on SALIENT (http://test.local.com)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(7, 6, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:24:\"SALIENT \"[your-subject]\"\";s:6:\"sender\";s:34:\"SALIENT <wordpress@test.local.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:108:\"Message Body:\n[your-message]\n\n-- \nThis email was sent from a contact form on SALIENT (http://test.local.com)\";s:18:\"additional_headers\";s:34:\"Reply-To: rusiru@leafcutter.com.au\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(8, 6, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:37:\"The email address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(9, 6, '_additional_settings', ''),
(10, 6, '_locale', 'en_AU'),
(11, 3, '_edit_lock', '1570010531:1'),
(12, 7, '_edit_last', '1'),
(13, 7, '_edit_lock', '1570015329:1'),
(14, 7, '_wp_page_template', 'default'),
(15, 7, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(16, 7, 'nectar-metabox-portfolio-display-sortable', 'off'),
(17, 7, '_nectar_full_screen_rows', 'off'),
(18, 7, '_nectar_full_screen_rows_animation', 'none'),
(19, 7, '_nectar_full_screen_rows_animation_speed', 'medium'),
(20, 7, '_nectar_full_screen_rows_overall_bg_color', ''),
(21, 7, '_nectar_full_screen_rows_anchors', 'off'),
(22, 7, '_nectar_full_screen_rows_mobile_disable', 'off'),
(23, 7, '_nectar_full_screen_rows_row_bg_animation', 'none'),
(24, 7, '_nectar_full_screen_rows_dot_navigation', 'tooltip'),
(25, 7, '_nectar_full_screen_rows_content_overflow', 'scrollbar'),
(26, 7, '_nectar_full_screen_rows_footer', 'none'),
(27, 7, '_nectar_slider_bg_type', 'image_bg'),
(28, 7, '_nectar_canvas_shapes', ''),
(29, 7, '_nectar_media_upload_webm', ''),
(30, 7, '_nectar_media_upload_mp4', ''),
(31, 7, '_nectar_media_upload_ogv', ''),
(32, 7, '_nectar_slider_preview_image', ''),
(33, 7, '_nectar_header_bg', ''),
(34, 7, '_nectar_header_parallax', 'off'),
(35, 7, '_nectar_header_box_roll', 'off'),
(36, 7, '_nectar_header_bg_height', ''),
(37, 7, '_nectar_header_fullscreen', 'off'),
(38, 7, '_nectar_header_title', ''),
(39, 7, '_nectar_header_subtitle', ''),
(40, 7, '_nectar_page_header_text-effect', 'none'),
(41, 7, '_nectar_particle_rotation_timing', ''),
(42, 7, '_nectar_particle_disable_explosion', 'off'),
(43, 7, '_nectar_page_header_alignment', 'left'),
(44, 7, '_nectar_page_header_alignment_v', 'middle'),
(45, 7, '_nectar_page_header_bg_alignment', 'center'),
(46, 7, '_nectar_header_bg_color', ''),
(47, 7, '_nectar_header_font_color', ''),
(48, 7, '_nectar_header_bg_overlay_color', ''),
(49, 7, '_wpb_vc_js_status', 'false'),
(50, 5, '_customize_restore_dismissed', '1'),
(51, 9, '_edit_lock', '1570010774:1'),
(52, 9, '_customize_restore_dismissed', '1'),
(53, 10, '_wp_trash_meta_status', 'publish'),
(54, 10, '_wp_trash_meta_time', '1570010795'),
(55, 12, '_edit_last', '1'),
(56, 12, '_nectar_slider_bg_type', 'image_bg'),
(57, 12, '_nectar_slider_image', ''),
(58, 12, '_nectar_media_upload_webm', ''),
(59, 12, '_nectar_media_upload_mp4', ''),
(60, 12, '_nectar_media_upload_ogv', ''),
(61, 12, '_nectar_slider_preview_image', ''),
(62, 12, '_nectar_slider_video_texture', 'on'),
(63, 12, '_nectar_slider_slide_bg_alignment', 'center'),
(64, 12, '_nectar_slider_slide_font_color', 'light'),
(65, 12, '_nectar_slider_heading', 'Beautiful &amp; Robust'),
(66, 12, '_nectar_slider_caption', 'Loaded with exclusive and beautiful features, Salient allows you to build a website that will have your visitors drooling from the moment they enter.'),
(67, 12, '_nectar_slider_caption_background', 'off'),
(68, 12, '_nectar_slider_slide_content_width_desktop', 'auto'),
(69, 12, '_nectar_slider_slide_content_width_tablet', 'auto'),
(70, 12, '_nectar_slider_bg_overlay_color', '#0a0a0a'),
(71, 12, '_nectar_slider_down_arrow', 'off'),
(72, 12, '_nectar_slider_link_type', 'button_links'),
(73, 12, '_nectar_slider_button', ''),
(74, 12, '_nectar_slider_button_url', ''),
(75, 12, '_nectar_slider_button_style', 'solid_color'),
(76, 12, '_nectar_slider_button_color', 'primary-color'),
(77, 12, '_nectar_slider_button_2', ''),
(78, 12, '_nectar_slider_button_url_2', ''),
(79, 12, '_nectar_slider_button_style_2', 'solid_color'),
(80, 12, '_nectar_slider_button_color_2', 'primary-color'),
(81, 12, '_nectar_slider_entire_link', ''),
(82, 12, '_nectar_slider_video_popup', ''),
(83, 12, '_nectar_slide_xpos_alignment', 'right'),
(84, 12, '_nectar_slide_ypos_alignment', 'middle'),
(85, 12, '_nectar_slider_slide_custom_class', 'slide1'),
(86, 12, '_edit_lock', '1570072159:1'),
(87, 13, '_wp_attached_file', 'woocommerce-placeholder.png'),
(88, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:21:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-600x600.png\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-900x604.png\";s:5:\"width\";i:900;s:6:\"height\";i:604;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"portfolio-thumb\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-600x403.png\";s:5:\"width\";i:600;s:6:\"height\";i:403;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-400x269.png\";s:5:\"width\";i:400;s:6:\"height\";i:269;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-140x140.png\";s:5:\"width\";i:140;s:6:\"height\";i:140;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"wide\";a:4:{s:4:\"file\";s:36:\"woocommerce-placeholder-1000x500.png\";s:5:\"width\";i:1000;s:6:\"height\";i:500;s:9:\"mime-type\";s:9:\"image/png\";}s:10:\"wide_small\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-670x335.png\";s:5:\"width\";i:670;s:6:\"height\";i:335;s:9:\"mime-type\";s:9:\"image/png\";}s:7:\"regular\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-500x500.png\";s:5:\"width\";i:500;s:6:\"height\";i:500;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-350x350.png\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:9:\"image/png\";}s:4:\"tall\";a:4:{s:4:\"file\";s:36:\"woocommerce-placeholder-500x1000.png\";s:5:\"width\";i:500;s:6:\"height\";i:1000;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"wide_tall\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1000x1000.png\";s:5:\"width\";i:1000;s:6:\"height\";i:1000;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"wide_photography\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-900x600.png\";s:5:\"width\";i:900;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"large_featured\";a:4:{s:4:\"file\";s:36:\"woocommerce-placeholder-1200x700.png\";s:5:\"width\";i:1200;s:6:\"height\";i:700;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"medium_featured\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-800x800.png\";s:5:\"width\";i:800;s:6:\"height\";i:800;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(124, 18, '_edit_last', '1'),
(125, 18, '_nectar_slider_bg_type', 'image_bg'),
(126, 18, '_nectar_slider_image', ''),
(127, 18, '_nectar_media_upload_webm', ''),
(128, 18, '_nectar_media_upload_mp4', ''),
(129, 18, '_nectar_media_upload_ogv', ''),
(130, 18, '_nectar_slider_preview_image', ''),
(131, 18, '_nectar_slider_video_texture', 'off'),
(132, 18, '_nectar_slider_slide_bg_alignment', 'center'),
(133, 18, '_nectar_slider_slide_font_color', 'light'),
(134, 18, '_nectar_slider_heading', 'Stream a movie'),
(135, 18, '_nectar_slider_caption', 'Stream unlimited movies and TV shows on <br />\r\n your phone, tablet, laptop, and TV without<br />\r\n paying more.'),
(136, 18, '_nectar_slider_caption_background', 'off'),
(137, 18, '_nectar_slider_slide_content_width_desktop', 'auto'),
(138, 18, '_nectar_slider_slide_content_width_tablet', 'auto'),
(139, 18, '_nectar_slider_bg_overlay_color', ''),
(140, 18, '_nectar_slider_down_arrow', 'off'),
(141, 18, '_nectar_slider_link_type', 'button_links'),
(142, 18, '_nectar_slider_button', 'VIEW MORE MOVIES'),
(143, 18, '_nectar_slider_button_url', ''),
(144, 18, '_nectar_slider_button_style', 'transparent'),
(145, 18, '_nectar_slider_button_color', 'primary-color'),
(146, 18, '_nectar_slider_button_2', 'LEARN ABOUT US'),
(147, 18, '_nectar_slider_button_url_2', ''),
(148, 18, '_nectar_slider_button_style_2', 'solid_color'),
(149, 18, '_nectar_slider_button_color_2', 'extra-color-2'),
(150, 18, '_nectar_slider_entire_link', ''),
(151, 18, '_nectar_slider_video_popup', ''),
(152, 18, '_nectar_slide_xpos_alignment', 'left'),
(153, 18, '_nectar_slide_ypos_alignment', 'middle'),
(154, 18, '_nectar_slider_slide_custom_class', ''),
(155, 18, '_edit_lock', '1570075889:1'),
(156, 19, '_wp_attached_file', '2019/10/salient-logo.png'),
(157, 19, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:251;s:6:\"height\";i:30;s:4:\"file\";s:24:\"2019/10/salient-logo.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"salient-logo-150x30.png\";s:5:\"width\";i:150;s:6:\"height\";i:30;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:23:\"salient-logo-100x30.png\";s:5:\"width\";i:100;s:6:\"height\";i:30;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:23:\"salient-logo-140x30.png\";s:5:\"width\";i:140;s:6:\"height\";i:30;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"salient-logo-150x30.png\";s:5:\"width\";i:150;s:6:\"height\";i:30;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"salient-logo-150x30.png\";s:5:\"width\";i:150;s:6:\"height\";i:30;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(158, 20, '_edit_last', '1'),
(159, 20, '_wp_page_template', 'default'),
(160, 20, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(161, 20, 'nectar-metabox-portfolio-display-sortable', 'off'),
(162, 20, '_nectar_full_screen_rows', 'off'),
(163, 20, '_nectar_full_screen_rows_animation', 'none'),
(164, 20, '_nectar_full_screen_rows_animation_speed', 'medium'),
(165, 20, '_nectar_full_screen_rows_overall_bg_color', ''),
(166, 20, '_nectar_full_screen_rows_anchors', 'off'),
(167, 20, '_nectar_full_screen_rows_mobile_disable', 'off'),
(168, 20, '_nectar_full_screen_rows_row_bg_animation', 'none'),
(169, 20, '_nectar_full_screen_rows_dot_navigation', 'tooltip'),
(170, 20, '_nectar_full_screen_rows_content_overflow', 'scrollbar'),
(171, 20, '_nectar_full_screen_rows_footer', 'none'),
(172, 20, '_nectar_slider_bg_type', 'image_bg'),
(173, 20, '_nectar_canvas_shapes', ''),
(174, 20, '_nectar_media_upload_webm', ''),
(175, 20, '_nectar_media_upload_mp4', ''),
(176, 20, '_nectar_media_upload_ogv', ''),
(177, 20, '_nectar_slider_preview_image', ''),
(178, 20, '_nectar_header_bg', ''),
(179, 20, '_nectar_header_parallax', 'off'),
(180, 20, '_nectar_header_box_roll', 'off'),
(181, 20, '_nectar_header_bg_height', ''),
(182, 20, '_nectar_header_fullscreen', 'off'),
(183, 20, '_nectar_header_title', ''),
(184, 20, '_nectar_header_subtitle', ''),
(185, 20, '_nectar_page_header_text-effect', 'none'),
(186, 20, '_nectar_particle_rotation_timing', ''),
(187, 20, '_nectar_particle_disable_explosion', 'off'),
(188, 20, '_nectar_page_header_alignment', 'left'),
(189, 20, '_nectar_page_header_alignment_v', 'middle'),
(190, 20, '_nectar_page_header_bg_alignment', 'center'),
(191, 20, '_nectar_header_bg_color', ''),
(192, 20, '_nectar_header_font_color', ''),
(193, 20, '_nectar_header_bg_overlay_color', ''),
(194, 20, '_wpb_vc_js_status', 'false'),
(195, 20, '_edit_lock', '1570072782:1'),
(196, 22, '_menu_item_type', 'post_type'),
(197, 22, '_menu_item_menu_item_parent', '0'),
(198, 22, '_menu_item_object_id', '20'),
(199, 22, '_menu_item_object', 'page'),
(200, 22, '_menu_item_target', ''),
(201, 22, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(202, 22, '_menu_item_xfn', ''),
(203, 22, '_menu_item_url', ''),
(205, 23, '_menu_item_type', 'post_type'),
(206, 23, '_menu_item_menu_item_parent', '0'),
(207, 23, '_menu_item_object_id', '7'),
(208, 23, '_menu_item_object', 'page'),
(209, 23, '_menu_item_target', ''),
(210, 23, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(211, 23, '_menu_item_xfn', ''),
(212, 23, '_menu_item_url', ''),
(214, 24, '_wp_trash_meta_status', 'publish'),
(215, 24, '_wp_trash_meta_time', '1570073077'),
(217, 25, '_customize_restore_dismissed', '1'),
(218, 26, '_edit_lock', '1570075656:1'),
(219, 26, '_wp_trash_meta_status', 'publish'),
(220, 26, '_wp_trash_meta_time', '1570075661'),
(221, 27, '_edit_lock', '1570075937:1'),
(222, 28, '_edit_last', '1'),
(223, 28, '_wp_page_template', 'default'),
(224, 28, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(225, 28, 'nectar-metabox-portfolio-display-sortable', 'off'),
(226, 28, '_nectar_full_screen_rows', 'off'),
(227, 28, '_nectar_full_screen_rows_animation', 'none'),
(228, 28, '_nectar_full_screen_rows_animation_speed', 'medium'),
(229, 28, '_nectar_full_screen_rows_overall_bg_color', ''),
(230, 28, '_nectar_full_screen_rows_anchors', 'off'),
(231, 28, '_nectar_full_screen_rows_mobile_disable', 'off'),
(232, 28, '_nectar_full_screen_rows_row_bg_animation', 'none'),
(233, 28, '_nectar_full_screen_rows_dot_navigation', 'tooltip'),
(234, 28, '_nectar_full_screen_rows_content_overflow', 'scrollbar'),
(235, 28, '_nectar_full_screen_rows_footer', 'none'),
(236, 28, '_nectar_slider_bg_type', 'image_bg'),
(237, 28, '_nectar_canvas_shapes', ''),
(238, 28, '_nectar_media_upload_webm', ''),
(239, 28, '_nectar_media_upload_mp4', ''),
(240, 28, '_nectar_media_upload_ogv', ''),
(241, 28, '_nectar_slider_preview_image', ''),
(242, 28, '_nectar_header_bg', ''),
(243, 28, '_nectar_header_parallax', 'off'),
(244, 28, '_nectar_header_box_roll', 'off'),
(245, 28, '_nectar_header_bg_height', ''),
(246, 28, '_nectar_header_fullscreen', 'off'),
(247, 28, '_nectar_header_title', ''),
(248, 28, '_nectar_header_subtitle', ''),
(249, 28, '_nectar_page_header_text-effect', 'none'),
(250, 28, '_nectar_particle_rotation_timing', ''),
(251, 28, '_nectar_particle_disable_explosion', 'off'),
(252, 28, '_nectar_page_header_alignment', 'left'),
(253, 28, '_nectar_page_header_alignment_v', 'middle'),
(254, 28, '_nectar_page_header_bg_alignment', 'center'),
(255, 28, '_nectar_header_bg_color', ''),
(256, 28, '_nectar_header_font_color', ''),
(257, 28, '_nectar_header_bg_overlay_color', ''),
(258, 28, '_wpb_vc_js_status', 'false'),
(259, 28, '_edit_lock', '1570075701:1'),
(260, 30, '_menu_item_type', 'post_type'),
(261, 30, '_menu_item_menu_item_parent', '0'),
(262, 30, '_menu_item_object_id', '20'),
(263, 30, '_menu_item_object', 'page'),
(264, 30, '_menu_item_target', ''),
(265, 30, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(266, 30, '_menu_item_xfn', ''),
(267, 30, '_menu_item_url', ''),
(269, 31, '_menu_item_type', 'post_type'),
(270, 31, '_menu_item_menu_item_parent', '0'),
(271, 31, '_menu_item_object_id', '28'),
(272, 31, '_menu_item_object', 'page'),
(273, 31, '_menu_item_target', ''),
(274, 31, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(275, 31, '_menu_item_xfn', ''),
(276, 31, '_menu_item_url', ''),
(278, 27, '_customize_restore_dismissed', '1'),
(279, 32, '_edit_lock', '1570075974:1'),
(280, 32, '_customize_restore_dismissed', '1'),
(281, 33, '_edit_lock', '1570076037:1'),
(282, 34, '_edit_last', '1'),
(283, 34, '_edit_lock', '1570076170:1'),
(284, 34, '_wp_page_template', 'default'),
(285, 34, 'nectar-metabox-portfolio-display', 'a:1:{i:0;s:3:\"all\";}'),
(286, 34, 'nectar-metabox-portfolio-display-sortable', 'off'),
(287, 34, '_nectar_full_screen_rows', 'off'),
(288, 34, '_nectar_full_screen_rows_animation', 'none'),
(289, 34, '_nectar_full_screen_rows_animation_speed', 'medium'),
(290, 34, '_nectar_full_screen_rows_overall_bg_color', ''),
(291, 34, '_nectar_full_screen_rows_anchors', 'off'),
(292, 34, '_nectar_full_screen_rows_mobile_disable', 'off'),
(293, 34, '_nectar_full_screen_rows_row_bg_animation', 'none'),
(294, 34, '_nectar_full_screen_rows_dot_navigation', 'tooltip'),
(295, 34, '_nectar_full_screen_rows_content_overflow', 'scrollbar'),
(296, 34, '_nectar_full_screen_rows_footer', 'none'),
(297, 34, '_nectar_slider_bg_type', 'image_bg'),
(298, 34, '_nectar_canvas_shapes', ''),
(299, 34, '_nectar_media_upload_webm', ''),
(300, 34, '_nectar_media_upload_mp4', ''),
(301, 34, '_nectar_media_upload_ogv', ''),
(302, 34, '_nectar_slider_preview_image', ''),
(303, 34, '_nectar_header_bg', ''),
(304, 34, '_nectar_header_parallax', 'off'),
(305, 34, '_nectar_header_box_roll', 'off'),
(306, 34, '_nectar_header_bg_height', ''),
(307, 34, '_nectar_header_fullscreen', 'off'),
(308, 34, '_nectar_header_title', ''),
(309, 34, '_nectar_header_subtitle', ''),
(310, 34, '_nectar_page_header_text-effect', 'none'),
(311, 34, '_nectar_particle_rotation_timing', ''),
(312, 34, '_nectar_particle_disable_explosion', 'off'),
(313, 34, '_nectar_page_header_alignment', 'left'),
(314, 34, '_nectar_page_header_alignment_v', 'middle'),
(315, 34, '_nectar_page_header_bg_alignment', 'center'),
(316, 34, '_nectar_header_bg_color', ''),
(317, 34, '_nectar_header_font_color', ''),
(318, 34, '_nectar_header_bg_overlay_color', ''),
(319, 34, '_wpb_vc_js_status', 'false'),
(320, 33, '_wp_trash_meta_status', 'publish'),
(321, 33, '_wp_trash_meta_time', '1570076074'),
(322, 37, '_menu_item_type', 'post_type'),
(323, 37, '_menu_item_menu_item_parent', '0'),
(324, 37, '_menu_item_object_id', '34'),
(325, 37, '_menu_item_object', 'page'),
(326, 37, '_menu_item_target', ''),
(327, 37, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(328, 37, '_menu_item_xfn', ''),
(329, 37, '_menu_item_url', ''),
(330, 36, '_wp_trash_meta_status', 'publish'),
(331, 36, '_wp_trash_meta_time', '1570076101'),
(332, 38, '_edit_lock', '1570076821:1'),
(339, 40, '_form', '[text* first_name placeholder \"First Name\"]\n[text* last_name placeholder \"Last Name\"]\n[email* email placeholder \"Email Address\"]\n[submit \"Sign up\"]'),
(340, 40, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:24:\"SALIENT \"[your-subject]\"\";s:6:\"sender\";s:34:\"SALIENT <wordpress@test.local.com>\";s:9:\"recipient\";s:24:\"rusiru@leafcutter.com.au\";s:4:\"body\";s:166:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis email was sent from a contact form on SALIENT (http://test.local.com)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(341, 40, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:24:\"SALIENT \"[your-subject]\"\";s:6:\"sender\";s:34:\"SALIENT <wordpress@test.local.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:108:\"Message Body:\n[your-message]\n\n-- \nThis email was sent from a contact form on SALIENT (http://test.local.com)\";s:18:\"additional_headers\";s:34:\"Reply-To: rusiru@leafcutter.com.au\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(342, 40, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:37:\"The email address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(343, 40, '_additional_settings', ''),
(344, 40, '_locale', 'en_AU'),
(345, 38, '_customize_restore_dismissed', '1'),
(346, 41, '_wp_trash_meta_status', 'publish'),
(347, 41, '_wp_trash_meta_time', '1570077075'),
(348, 46, '_action_manager_schedule', 'O:30:\"ActionScheduler_SimpleSchedule\":1:{s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1570079952;}'),
(349, 48, '_edit_last', '1'),
(350, 48, '_edit_lock', '1570175346:1'),
(351, 48, '_nectar_portfolio_extra_content', '<p>[vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]&nbsp;</p>\n<p style=\"text-align: center; font-size: 18px;\"><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<p>[/vc_column_text][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_btn size=\"jumbo\" button_style=\"see-through-2\" hover_text_color_override=\"#ffffff\" icon_family=\"none\" el_class=\"rent\" text=\"Rent ($9.99)\" margin_top=\"32\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_btn size=\"jumbo\" button_style=\"regular\" button_color_2=\"Accent-Color\" icon_family=\"none\" el_class=\"buy\" text=\"BUY ($19.99)\" margin_top=\"32\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]Cast[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][clients columns=\"6\" hover_effect=\"opacity\" additional_padding=\"none\"][client image=\"13\" title=\"Client\" id=\"1570174753218-0\" tab_id=\"1570174753219-4\"][/client][client image=\"13\" title=\"Client\" id=\"1570174753218-0\" tab_id=\"1570174753219-4\"][/client][client image=\"13\" title=\"Client\" id=\"1570174753218-0\" tab_id=\"1570174753219-4\"][/client][client image=\"13\" title=\"Client\" id=\"1570174753218-0\" tab_id=\"1570174753219-4\"][/client][client image=\"13\" title=\"Client\" id=\"1570174753218-0\" tab_id=\"1570174753219-4\"][/client][/clients][/vc_column][/vc_row][vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#969696\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/6\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"2/3\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]</p>\n<div class=\"wpb_text_column wpb_content_element vc_custom_1568195631229\">\n<div class=\"wpb_wrapper\">\n<h2 style=\"text-align: center;\">Watch Trailer</h2>\n<p style=\"text-align: center;\">Phasellus at vulputate urna. Suspendisse tincidunt diam sem, ac varius justo imperdiet sit amet quisque. Fringilla nulla suscipit aliquet.</p>\n</div>\n</div>\n<div class=\"wpb_video_widget wpb_content_element vc_clearfix vc_video-aspect-ratio-169 vc_video-el-width-100 vc_video-align-left\">\n<div class=\"wpb_wrapper\" style=\"text-align: center;\"></div>\n</div>\n<p>[/vc_column_text][vc_video][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/6\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]</p>\n<h2>Testimonials</h2>\n<p>[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][testimonial_slider style=\"default\" star_rating_color=\"accent-color\"][testimonial star_rating=\"80%\" title=\"Testimonial\" id=\"1570174753821-10\" tab_id=\"1570174753823-3\" name=\"Person Name\" quote=\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\"][testimonial star_rating=\"none\" title=\"Testimonial\" id=\"1570174753912-9\" tab_id=\"1570174753914-2\" name=\"Person Name\" quote=\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\"][testimonial star_rating=\"701%\" title=\"Testimonial\" id=\"1570174753995-3\" tab_id=\"1570174753996-8\" name=\"Perdson Name\" quote=\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\"][testimonial star_rating=\"none\" title=\"Testimonial\" id=\"1570174754102-8\" tab_id=\"1570174754104-3\"][testimonial star_rating=\"none\" title=\"Testimonial\" id=\"1570174754193-3\" tab_id=\"1570174754194-5\"][/testimonial_slider][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][social_buttons nectar_love=\"true\" facebook=\"true\" twitter=\"true\" google_plus=\"true\" linkedin=\"true\" pinterest=\"true\"][/vc_column][/vc_row]</p>\n'),
(352, 48, '_nectar_portfolio_item_layout', 'enabled'),
(353, 48, '_nectar_portfolio_custom_grid_item', 'off'),
(354, 48, '_nectar_portfolio_custom_grid_item_content', ''),
(355, 48, '_nectar_portfolio_custom_thumbnail', ''),
(356, 48, '_nectar_hide_featured', 'on'),
(357, 48, '_portfolio_item_masonry_sizing', 'wide_tall'),
(358, 48, '_portfolio_item_masonry_content_pos', 'middle'),
(359, 48, '_nectar_external_project_url', ''),
(360, 48, 'nectar-metabox-portfolio-parent-override', 'default'),
(361, 48, '_nectar_project_excerpt', ''),
(362, 48, '_nectar_project_accent_color', ''),
(363, 48, '_nectar_project_title_color', ''),
(364, 48, '_nectar_project_subtitle_color', ''),
(365, 48, '_nectar_project_css_class', ''),
(366, 48, '_nectar_slider_bg_type', 'image_bg'),
(367, 48, '_nectar_media_upload_webm', ''),
(368, 48, '_nectar_media_upload_mp4', ''),
(369, 48, '_nectar_media_upload_ogv', ''),
(370, 48, '_nectar_slider_preview_image', ''),
(371, 48, '_nectar_header_bg', ''),
(372, 48, '_nectar_header_parallax', 'off'),
(373, 48, '_nectar_header_bg_height', ''),
(374, 48, '_nectar_header_fullscreen', 'off'),
(375, 48, '_nectar_page_header_bg_alignment', 'center'),
(376, 48, '_nectar_header_bg_color', '#000000'),
(377, 48, '_nectar_header_bg_overlay_color', ''),
(378, 48, '_nectar_header_subtitle', 'This is the movie subtitle'),
(379, 48, '_nectar_header_font_color', '#f7f7f7'),
(380, 48, '_nectar_video_m4v', ''),
(381, 48, '_nectar_video_ogv', ''),
(382, 48, '_nectar_video_poster', ''),
(383, 48, '_nectar_video_embed', ''),
(384, 48, '_wpb_vc_js_status', 'true'),
(385, 50, '_wp_attached_file', '2019/10/1550457733.gif'),
(386, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:333;s:4:\"file\";s:22:\"2019/10/1550457733.gif\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"1550457733-150x150.gif\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/gif\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"1550457733-300x200.gif\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/gif\";}s:21:\"portfolio-thumb_small\";a:4:{s:4:\"file\";s:22:\"1550457733-400x269.gif\";s:5:\"width\";i:400;s:6:\"height\";i:269;s:9:\"mime-type\";s:9:\"image/gif\";}s:16:\"portfolio-widget\";a:4:{s:4:\"file\";s:22:\"1550457733-100x100.gif\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/gif\";}s:19:\"nectar_small_square\";a:4:{s:4:\"file\";s:22:\"1550457733-140x140.gif\";s:5:\"width\";i:140;s:6:\"height\";i:140;s:9:\"mime-type\";s:9:\"image/gif\";}s:7:\"regular\";a:4:{s:4:\"file\";s:22:\"1550457733-500x333.gif\";s:5:\"width\";i:500;s:6:\"height\";i:333;s:9:\"mime-type\";s:9:\"image/gif\";}s:13:\"regular_small\";a:4:{s:4:\"file\";s:22:\"1550457733-350x333.gif\";s:5:\"width\";i:350;s:6:\"height\";i:333;s:9:\"mime-type\";s:9:\"image/gif\";}s:4:\"tall\";a:4:{s:4:\"file\";s:22:\"1550457733-500x333.gif\";s:5:\"width\";i:500;s:6:\"height\";i:333;s:9:\"mime-type\";s:9:\"image/gif\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"1550457733-300x300.gif\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/gif\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"1550457733-150x150.gif\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/gif\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"1550457733-300x300.gif\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/gif\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"1550457733-150x150.gif\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/gif\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(387, 48, '_nectar_love', '1'),
(388, 48, '_thumbnail_id', '50'),
(389, 48, '_oembed_a563b557bea91623c0e6a95b4544c0b6', '<iframe title=\"Timelapse - Lighthouse (Oct 2012)\" src=\"https://player.vimeo.com/video/51589652?dnt=1&amp;app_id=122963\" width=\"1080\" height=\"608\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen></iframe>'),
(390, 48, '_oembed_time_a563b557bea91623c0e6a95b4544c0b6', '1570082600'),
(391, 57, '_edit_last', '1'),
(392, 57, '_edit_lock', '1570099538:1'),
(393, 57, '_thumbnail_id', '50'),
(394, 57, '_nectar_portfolio_extra_content', ''),
(395, 57, '_nectar_portfolio_item_layout', 'disabled'),
(396, 57, '_nectar_portfolio_custom_grid_item', 'off'),
(397, 57, '_nectar_portfolio_custom_grid_item_content', ''),
(398, 57, '_nectar_portfolio_custom_thumbnail', ''),
(399, 57, '_nectar_hide_featured', 'off'),
(400, 57, '_portfolio_item_masonry_sizing', 'regular'),
(401, 57, '_portfolio_item_masonry_content_pos', 'middle'),
(402, 57, '_nectar_external_project_url', ''),
(403, 57, 'nectar-metabox-portfolio-parent-override', 'default'),
(404, 57, '_nectar_project_excerpt', ''),
(405, 57, '_nectar_project_accent_color', ''),
(406, 57, '_nectar_project_title_color', ''),
(407, 57, '_nectar_project_subtitle_color', ''),
(408, 57, '_nectar_project_css_class', ''),
(409, 57, '_nectar_slider_bg_type', 'image_bg'),
(410, 57, '_nectar_media_upload_webm', ''),
(411, 57, '_nectar_media_upload_mp4', ''),
(412, 57, '_nectar_media_upload_ogv', ''),
(413, 57, '_nectar_slider_preview_image', ''),
(414, 57, '_nectar_header_bg', ''),
(415, 57, '_nectar_header_parallax', 'off'),
(416, 57, '_nectar_header_bg_height', ''),
(417, 57, '_nectar_header_fullscreen', 'off'),
(418, 57, '_nectar_page_header_bg_alignment', 'center'),
(419, 57, '_nectar_header_bg_color', ''),
(420, 57, '_nectar_header_bg_overlay_color', ''),
(421, 57, '_nectar_header_subtitle', ''),
(422, 57, '_nectar_header_font_color', ''),
(423, 57, '_nectar_video_m4v', ''),
(424, 57, '_nectar_video_ogv', ''),
(425, 57, '_nectar_video_poster', ''),
(426, 57, '_nectar_video_embed', ''),
(427, 57, '_wpb_vc_js_status', 'false'),
(428, 59, '_edit_last', '1'),
(429, 59, '_edit_lock', '1570083453:1'),
(430, 59, '_thumbnail_id', '50'),
(431, 59, '_nectar_portfolio_extra_content', ''),
(432, 59, '_nectar_portfolio_item_layout', 'disabled'),
(433, 59, '_nectar_portfolio_custom_grid_item', 'off'),
(434, 59, '_nectar_portfolio_custom_grid_item_content', ''),
(435, 59, '_nectar_portfolio_custom_thumbnail', ''),
(436, 59, '_nectar_hide_featured', 'off'),
(437, 59, '_portfolio_item_masonry_sizing', 'regular'),
(438, 59, '_portfolio_item_masonry_content_pos', 'middle'),
(439, 59, '_nectar_external_project_url', ''),
(440, 59, 'nectar-metabox-portfolio-parent-override', 'default'),
(441, 59, '_nectar_project_excerpt', ''),
(442, 59, '_nectar_project_accent_color', ''),
(443, 59, '_nectar_project_title_color', ''),
(444, 59, '_nectar_project_subtitle_color', ''),
(445, 59, '_nectar_project_css_class', ''),
(446, 59, '_nectar_slider_bg_type', 'image_bg'),
(447, 59, '_nectar_media_upload_webm', ''),
(448, 59, '_nectar_media_upload_mp4', ''),
(449, 59, '_nectar_media_upload_ogv', ''),
(450, 59, '_nectar_slider_preview_image', ''),
(451, 59, '_nectar_header_bg', ''),
(452, 59, '_nectar_header_parallax', 'off'),
(453, 59, '_nectar_header_bg_height', ''),
(454, 59, '_nectar_header_fullscreen', 'off'),
(455, 59, '_nectar_page_header_bg_alignment', 'center'),
(456, 59, '_nectar_header_bg_color', ''),
(457, 59, '_nectar_header_bg_overlay_color', ''),
(458, 59, '_nectar_header_subtitle', ''),
(459, 59, '_nectar_header_font_color', ''),
(460, 59, '_nectar_video_m4v', ''),
(461, 59, '_nectar_video_ogv', ''),
(462, 59, '_nectar_video_poster', ''),
(463, 59, '_nectar_video_embed', ''),
(464, 59, '_wpb_vc_js_status', 'false'),
(465, 59, '_nectar_love', '1'),
(466, 57, '_nectar_love', '0'),
(467, 61, '_edit_last', '1'),
(468, 61, '_edit_lock', '1570083543:1'),
(469, 61, '_thumbnail_id', '50'),
(470, 61, '_nectar_portfolio_extra_content', ''),
(471, 61, '_nectar_portfolio_item_layout', 'disabled'),
(472, 61, '_nectar_portfolio_custom_grid_item', 'off'),
(473, 61, '_nectar_portfolio_custom_grid_item_content', ''),
(474, 61, '_nectar_portfolio_custom_thumbnail', ''),
(475, 61, '_nectar_hide_featured', 'off'),
(476, 61, '_portfolio_item_masonry_sizing', 'regular'),
(477, 61, '_portfolio_item_masonry_content_pos', 'middle'),
(478, 61, '_nectar_external_project_url', ''),
(479, 61, 'nectar-metabox-portfolio-parent-override', 'default'),
(480, 61, '_nectar_project_excerpt', ''),
(481, 61, '_nectar_project_accent_color', ''),
(482, 61, '_nectar_project_title_color', ''),
(483, 61, '_nectar_project_subtitle_color', ''),
(484, 61, '_nectar_project_css_class', ''),
(485, 61, '_nectar_slider_bg_type', 'image_bg'),
(486, 61, '_nectar_media_upload_webm', ''),
(487, 61, '_nectar_media_upload_mp4', ''),
(488, 61, '_nectar_media_upload_ogv', ''),
(489, 61, '_nectar_slider_preview_image', ''),
(490, 61, '_nectar_header_bg', ''),
(491, 61, '_nectar_header_parallax', 'off'),
(492, 61, '_nectar_header_bg_height', ''),
(493, 61, '_nectar_header_fullscreen', 'off'),
(494, 61, '_nectar_page_header_bg_alignment', 'center'),
(495, 61, '_nectar_header_bg_color', ''),
(496, 61, '_nectar_header_bg_overlay_color', ''),
(497, 61, '_nectar_header_subtitle', ''),
(498, 61, '_nectar_header_font_color', ''),
(499, 61, '_nectar_video_m4v', ''),
(500, 61, '_nectar_video_ogv', ''),
(501, 61, '_nectar_video_poster', ''),
(502, 61, '_nectar_video_embed', ''),
(503, 61, '_wpb_vc_js_status', 'false'),
(504, 63, '_edit_last', '1'),
(505, 63, '_edit_lock', '1570174588:1'),
(506, 61, '_nectar_love', '1'),
(507, 63, '_thumbnail_id', '50'),
(508, 63, '_nectar_portfolio_extra_content', ''),
(509, 63, '_nectar_portfolio_item_layout', 'disabled'),
(510, 63, '_nectar_portfolio_custom_grid_item', 'off'),
(511, 63, '_nectar_portfolio_custom_grid_item_content', ''),
(512, 63, '_nectar_portfolio_custom_thumbnail', ''),
(513, 63, '_nectar_hide_featured', 'off'),
(514, 63, '_portfolio_item_masonry_sizing', 'regular'),
(515, 63, '_portfolio_item_masonry_content_pos', 'middle'),
(516, 63, '_nectar_external_project_url', ''),
(517, 63, 'nectar-metabox-portfolio-parent-override', 'default'),
(518, 63, '_nectar_project_excerpt', ''),
(519, 63, '_nectar_project_accent_color', ''),
(520, 63, '_nectar_project_title_color', ''),
(521, 63, '_nectar_project_subtitle_color', ''),
(522, 63, '_nectar_project_css_class', ''),
(523, 63, '_nectar_slider_bg_type', 'image_bg'),
(524, 63, '_nectar_media_upload_webm', ''),
(525, 63, '_nectar_media_upload_mp4', ''),
(526, 63, '_nectar_media_upload_ogv', ''),
(527, 63, '_nectar_slider_preview_image', ''),
(528, 63, '_nectar_header_bg', ''),
(529, 63, '_nectar_header_parallax', 'off'),
(530, 63, '_nectar_header_bg_height', ''),
(531, 63, '_nectar_header_fullscreen', 'off'),
(532, 63, '_nectar_page_header_bg_alignment', 'center'),
(533, 63, '_nectar_header_bg_color', ''),
(534, 63, '_nectar_header_bg_overlay_color', ''),
(535, 63, '_nectar_header_subtitle', ''),
(536, 63, '_nectar_header_font_color', ''),
(537, 63, '_nectar_video_m4v', ''),
(538, 63, '_nectar_video_ogv', ''),
(539, 63, '_nectar_video_poster', ''),
(540, 63, '_nectar_video_embed', ''),
(541, 63, '_wpb_vc_js_status', 'false'),
(542, 65, '_edit_last', '1'),
(543, 65, '_edit_lock', '1570099672:1'),
(544, 65, '_thumbnail_id', '50'),
(545, 65, '_nectar_portfolio_extra_content', ''),
(546, 65, '_nectar_portfolio_item_layout', 'disabled'),
(547, 65, '_nectar_portfolio_custom_grid_item', 'off'),
(548, 65, '_nectar_portfolio_custom_grid_item_content', ''),
(549, 65, '_nectar_portfolio_custom_thumbnail', ''),
(550, 65, '_nectar_hide_featured', 'off'),
(551, 65, '_portfolio_item_masonry_sizing', 'regular'),
(552, 65, '_portfolio_item_masonry_content_pos', 'middle'),
(553, 65, '_nectar_external_project_url', ''),
(554, 65, 'nectar-metabox-portfolio-parent-override', 'default'),
(555, 65, '_nectar_project_excerpt', ''),
(556, 65, '_nectar_project_accent_color', ''),
(557, 65, '_nectar_project_title_color', ''),
(558, 65, '_nectar_project_subtitle_color', ''),
(559, 65, '_nectar_project_css_class', ''),
(560, 65, '_nectar_slider_bg_type', 'image_bg'),
(561, 65, '_nectar_media_upload_webm', ''),
(562, 65, '_nectar_media_upload_mp4', ''),
(563, 65, '_nectar_media_upload_ogv', ''),
(564, 65, '_nectar_slider_preview_image', ''),
(565, 65, '_nectar_header_bg', ''),
(566, 65, '_nectar_header_parallax', 'off'),
(567, 65, '_nectar_header_bg_height', ''),
(568, 65, '_nectar_header_fullscreen', 'off'),
(569, 65, '_nectar_page_header_bg_alignment', 'center'),
(570, 65, '_nectar_header_bg_color', ''),
(571, 65, '_nectar_header_bg_overlay_color', ''),
(572, 65, '_nectar_header_subtitle', ''),
(573, 65, '_nectar_header_font_color', ''),
(574, 65, '_nectar_video_m4v', ''),
(575, 65, '_nectar_video_ogv', ''),
(576, 65, '_nectar_video_poster', ''),
(577, 65, '_nectar_video_embed', ''),
(578, 65, '_wpb_vc_js_status', 'false'),
(579, 65, '_nectar_love', '1'),
(580, 63, '_nectar_love', '1'),
(581, 40, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(583, 72, '_dp_original', '1'),
(584, 72, '_edit_lock', '1570099924:1'),
(585, 72, '_edit_last', '1'),
(588, 72, '_nectar_gallery_slider', 'off'),
(589, 72, '_nectar_quote_author', ''),
(590, 72, '_nectar_quote', ''),
(591, 72, '_nectar_link', '');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(592, 72, '_nectar_video_m4v', ''),
(593, 72, '_nectar_video_ogv', ''),
(594, 72, '_nectar_video_poster', ''),
(595, 72, '_nectar_video_embed', ''),
(596, 72, '_nectar_audio_mp3', ''),
(597, 72, '_nectar_audio_ogg', ''),
(598, 72, '_nectar_header_bg', ''),
(599, 72, '_nectar_header_parallax', 'off'),
(600, 72, '_nectar_header_bg_height', ''),
(601, 72, '_nectar_page_header_bg_alignment', 'top'),
(602, 72, '_nectar_header_bg_color', ''),
(603, 72, '_nectar_header_font_color', ''),
(604, 72, '_wpb_vc_js_status', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-10-02 08:24:37', '2019-10-02 08:24:37', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2019-10-02 08:24:37', '2019-10-02 08:24:37', '', 0, 'http://localhost/wordpress-template/public/?p=1', 0, 'post', '', 1),
(2, 1, '2019-10-02 08:24:37', '2019-10-02 08:24:37', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost/wordpress-template/public/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2019-10-02 08:24:37', '2019-10-02 08:24:37', '', 0, 'http://localhost/wordpress-template/public/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-10-02 08:24:37', '2019-10-02 08:24:37', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost/wordpress-template/public.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymised string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognise and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2019-10-02 08:24:37', '2019-10-02 08:24:37', '', 0, 'http://localhost/wordpress-template/public/?page_id=3', 0, 'page', '', 0),
(4, 1, '2019-10-02 08:25:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-10-02 08:25:02', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress-template/public/?p=4', 0, 'post', '', 0),
(5, 1, '2019-10-02 09:58:51', '0000-00-00 00:00:00', '{\n    \"show_on_front\": {\n        \"value\": \"posts\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-02 09:58:51\"\n    },\n    \"page_on_front\": {\n        \"value\": \"0\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-02 09:58:51\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '744bdf31-b70e-43bb-9651-b074be556e1d', '', '', '2019-10-02 09:58:51', '0000-00-00 00:00:00', '', 0, 'http://test.local.com/?p=5', 0, 'customize_changeset', '', 0),
(6, 1, '2019-10-02 10:01:20', '2019-10-02 10:01:20', '<label> Your Name (required)\r\n    [text* your-name] </label>\r\n\r\n<label> Your Email (required)\r\n    [email* your-email] </label>\r\n\r\n<label> Subject\r\n    [text your-subject] </label>\r\n\r\n<label> Your Message\r\n    [textarea your-message] </label>\r\n\r\n[submit \"Send\"]\n1\nSALIENT \"[your-subject]\"\nrusiru@test.local.com\nrusiru@leafcutter.com.au\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis email was sent from a contact form on SALIENT (http://test.local.com)\nReply-To: [your-email]\n\n\n\n\nSALIENT \"[your-subject]\"\nSALIENT <wordpress@test.local.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis email was sent from a contact form on SALIENT (http://test.local.com)\nReply-To: rusiru@leafcutter.com.au\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe email address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2019-10-03 08:14:22', '2019-10-03 08:14:22', '', 0, 'http://test.local.com/?post_type=wpcf7_contact_form&#038;p=6', 0, 'wpcf7_contact_form', '', 0),
(7, 1, '2019-10-02 10:05:26', '2019-10-02 10:05:26', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]Movies[/heading][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"fullwidth\" category=\"all\" starting_category=\"default\" project_style=\"1\" item_spacing=\"default\" enable_sortable=\"true\" horizontal_filters=\"true\" filter_alignment=\"center\" filter_color=\"extra-color-3\" enable_pagination=\"true\" pagination_type=\"infinite_scroll\" load_in_animation=\"fade_in_from_bottom\" projects_per_page=\"10\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2019-10-03 06:29:37', '2019-10-03 06:29:37', '', 0, 'http://test.local.com/?page_id=7', 0, 'page', '', 0),
(8, 1, '2019-10-02 10:05:26', '2019-10-02 10:05:26', '', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-02 10:05:26', '2019-10-02 10:05:26', '', 7, 'http://test.local.com/2019/10/02/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2019-10-02 10:06:14', '0000-00-00 00:00:00', '{\n    \"show_on_front\": {\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-02 10:06:14\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '052d69ba-aa89-4f54-9750-4635bce4a66d', '', '', '2019-10-02 10:06:14', '0000-00-00 00:00:00', '', 0, 'http://test.local.com/?p=9', 0, 'customize_changeset', '', 0),
(10, 1, '2019-10-02 10:06:34', '2019-10-02 10:06:34', '{\n    \"show_on_front\": {\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-02 10:06:34\"\n    },\n    \"page_on_front\": {\n        \"value\": \"7\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-02 10:06:34\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'cedfc1cb-ea1c-4d4a-9f4f-4ba5f735dca5', '', '', '2019-10-02 10:06:34', '2019-10-02 10:06:34', '', 0, 'http://test.local.com/2019/10/02/cedfc1cb-ea1c-4d4a-9f4f-4ba5f735dca5/', 0, 'customize_changeset', '', 0),
(11, 1, '2019-10-02 10:23:00', '2019-10-02 10:23:00', '<p>[vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider overall_style=\"classic\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-autosave-v1', '', '', '2019-10-02 10:23:00', '2019-10-02 10:23:00', '', 7, 'http://test.local.com/2019/10/02/7-autosave-v1/', 0, 'revision', '', 0),
(12, 1, '2019-10-02 10:26:46', '2019-10-02 10:26:46', '', 'Auto Draft', '', 'publish', 'closed', 'closed', '', 'auto-draft', '', '', '2019-10-02 10:42:47', '2019-10-02 10:42:47', '', 0, 'http://test.local.com/?post_type=nectar_slider&#038;p=12', 0, 'nectar_slider', '', 0),
(13, 1, '2019-10-02 10:28:12', '2019-10-02 10:28:12', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2019-10-02 10:28:12', '2019-10-02 10:28:12', '', 0, 'http://test.local.com/wp-content/uploads/2019/10/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(14, 1, '2019-10-02 10:33:53', '2019-10-02 10:33:53', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" overall_style=\"classic\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-02 10:33:53', '2019-10-02 10:33:53', '', 7, 'http://test.local.com/2019/10/02/7-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2019-10-02 10:39:08', '2019-10-02 10:39:08', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-02 10:39:08', '2019-10-02 10:39:08', '', 7, 'http://test.local.com/2019/10/02/7-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2019-10-03 02:59:17', '2019-10-03 02:59:17', '', '', '', 'inherit', 'closed', 'closed', '', '12-autosave-v1', '', '', '2019-10-03 02:59:17', '2019-10-03 02:59:17', '', 12, 'http://test.local.com/2019/10/03/12-autosave-v1/', 0, 'revision', '', 0),
(18, 1, '2019-10-03 03:06:50', '2019-10-03 03:06:50', '', 'Auto Draft', '', 'publish', 'closed', 'closed', '', 'auto-draft-2', '', '', '2019-10-03 03:11:21', '2019-10-03 03:11:21', '', 0, 'http://test.local.com/?post_type=nectar_slider&#038;p=18', 0, 'nectar_slider', '', 0),
(19, 1, '2019-10-03 03:16:57', '2019-10-03 03:16:57', '', 'salient-logo', '', 'inherit', 'open', 'closed', '', 'salient-logo', '', '', '2019-10-03 03:16:57', '2019-10-03 03:16:57', '', 0, 'http://test.local.com/wp-content/uploads/2019/10/salient-logo.png', 0, 'attachment', 'image/png', 0),
(20, 1, '2019-10-03 03:21:43', '2019-10-03 03:21:43', '<p>[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#969696\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]</p>\n<h1>Contact Us</h1>\n<p>[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]</p>\n<h2 style=\"text-align: center;\"><strong>Get in Touch </strong></h2>\n<p style=\"text-align: center;\">We would like to hear you feedback.</p>\n<p>[/vc_column_text][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_raw_html]JTVCY29udGFjdC1mb3JtLTclMjBpZCUzRCUyMjYlMjIlMjB0aXRsZSUzRCUyMkNvbnRhY3QlMjBmb3JtJTIwMSUyMiU1RA==[/vc_raw_html][/vc_column][/vc_row]</p>\n', 'Contact Us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2019-10-03 08:11:08', '2019-10-03 08:11:08', '', 0, 'http://test.local.com/?page_id=20', 0, 'page', '', 0),
(21, 1, '2019-10-03 03:21:43', '2019-10-03 03:21:43', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2019-10-03 03:21:43', '2019-10-03 03:21:43', '', 20, 'http://test.local.com/2019/10/03/20-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2019-10-03 03:22:23', '2019-10-03 03:22:23', ' ', '', '', 'publish', 'closed', 'closed', '', '22', '', '', '2019-10-03 03:22:23', '2019-10-03 03:22:23', '', 0, 'http://test.local.com/?p=22', 2, 'nav_menu_item', '', 0),
(23, 1, '2019-10-03 03:22:22', '2019-10-03 03:22:22', ' ', '', '', 'publish', 'closed', 'closed', '', '23', '', '', '2019-10-03 03:22:22', '2019-10-03 03:22:22', '', 0, 'http://test.local.com/?p=23', 1, 'nav_menu_item', '', 0),
(24, 1, '2019-10-03 03:24:37', '2019-10-03 03:24:37', '{\n    \"leafcutter::nav_menu_locations[top_nav]\": {\n        \"value\": 17,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 03:24:37\"\n    },\n    \"leafcutter::nav_menu_locations[secondary_nav]\": {\n        \"value\": 0,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 03:24:37\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'fc8d55b4-7cc1-48ab-ba7a-17129a5237f0', '', '', '2019-10-03 03:24:37', '2019-10-03 03:24:37', '', 0, 'http://test.local.com/2019/10/03/fc8d55b4-7cc1-48ab-ba7a-17129a5237f0/', 0, 'customize_changeset', '', 0),
(25, 1, '2019-10-03 03:29:42', '0000-00-00 00:00:00', '{\n    \"leafcutter::nav_menu_locations[top_nav]\": {\n        \"value\": 17,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 03:29:42\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'f3c3b35b-5ad2-410f-8f23-6bff7fb68768', '', '', '2019-10-03 03:29:42', '0000-00-00 00:00:00', '', 0, 'http://test.local.com/?p=25', 0, 'customize_changeset', '', 0),
(26, 1, '2019-10-03 04:07:41', '2019-10-03 04:07:41', '{\n    \"sidebars_widgets[footer-area-1]\": {\n        \"value\": [\n            \"recent-posts-4\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:07:18\"\n    },\n    \"widget_recent-posts[4]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YTozOntzOjU6InRpdGxlIjtzOjE5OiJGUkVTSCBGUk9NIFRIRSBCTE9HIjtzOjY6Im51bWJlciI7aTozO3M6OToic2hvd19kYXRlIjtiOjA7fQ==\",\n            \"title\": \"FRESH FROM THE BLOG\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"84383851e6ae1873f37e6a930eebe589\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:07:35\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '5657a5ff-de17-440f-a628-75848ffd8752', '', '', '2019-10-03 04:07:41', '2019-10-03 04:07:41', '', 0, 'http://test.local.com/?p=26', 0, 'customize_changeset', '', 0),
(27, 1, '2019-10-03 04:08:59', '0000-00-00 00:00:00', '{\n    \"sidebars_widgets[footer-area-2]\": {\n        \"value\": [\n            \"nav_menu-3\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:08:59\"\n    },\n    \"widget_nav_menu[3]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YToxOntzOjU6InRpdGxlIjtzOjExOiJRdWljayBMaW5rcyI7fQ==\",\n            \"title\": \"Quick Links\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"0839ab0f21e5d713c762173b538ecf45\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:08:59\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'bdd580d4-b622-4bf2-98f9-425a6677fa64', '', '', '2019-10-03 04:08:59', '0000-00-00 00:00:00', '', 0, 'http://test.local.com/?p=27', 0, 'customize_changeset', '', 0),
(28, 1, '2019-10-03 04:10:07', '2019-10-03 04:10:07', '', 'Terms & Conditions', '', 'publish', 'closed', 'closed', '', 'terms-conditions', '', '', '2019-10-03 04:10:07', '2019-10-03 04:10:07', '', 0, 'http://test.local.com/?page_id=28', 0, 'page', '', 0),
(29, 1, '2019-10-03 04:10:07', '2019-10-03 04:10:07', '', 'Terms & Conditions', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2019-10-03 04:10:07', '2019-10-03 04:10:07', '', 28, 'http://test.local.com/2019/10/03/28-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2019-10-03 04:12:48', '2019-10-03 04:12:48', ' ', '', '', 'publish', 'closed', 'closed', '', '30', '', '', '2019-10-03 04:12:48', '2019-10-03 04:12:48', '', 0, 'http://test.local.com/?p=30', 1, 'nav_menu_item', '', 0),
(31, 1, '2019-10-03 04:12:49', '2019-10-03 04:12:49', ' ', '', '', 'publish', 'closed', 'closed', '', '31', '', '', '2019-10-03 04:12:49', '2019-10-03 04:12:49', '', 0, 'http://test.local.com/?p=31', 2, 'nav_menu_item', '', 0),
(32, 1, '2019-10-03 04:12:53', '0000-00-00 00:00:00', '{\n    \"sidebars_widgets[footer-area-2]\": {\n        \"value\": [\n            \"nav_menu-3\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:12:53\"\n    },\n    \"widget_nav_menu[3]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YToxOntzOjg6Im5hdl9tZW51IjtpOjE4O30=\",\n            \"title\": \"\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"bf79e3cf5f69af4db580b0df31138508\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:12:53\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'b6ec1ae4-518e-499f-9c77-dc9f41ad6113', '', '', '2019-10-03 04:12:53', '0000-00-00 00:00:00', '', 0, 'http://test.local.com/?p=32', 0, 'customize_changeset', '', 0),
(33, 1, '2019-10-03 04:14:34', '2019-10-03 04:14:34', '{\n    \"sidebars_widgets[footer-area-2]\": {\n        \"value\": [\n            \"nav_menu-3\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:13:33\"\n    },\n    \"widget_nav_menu[3]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YToyOntzOjU6InRpdGxlIjtzOjEwOiJRdWljayBNZW51IjtzOjg6Im5hdl9tZW51IjtpOjE4O30=\",\n            \"title\": \"Quick Menu\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"9a28d23e3541ef45346c52da9b117aaa\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:13:33\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '2741b502-c258-4633-ab51-cc9b65cb4aec', '', '', '2019-10-03 04:14:34', '2019-10-03 04:14:34', '', 0, 'http://test.local.com/?p=33', 0, 'customize_changeset', '', 0),
(34, 1, '2019-10-03 04:14:23', '2019-10-03 04:14:23', '', 'Privacy Policy', '', 'publish', 'closed', 'closed', '', 'privacy-policy-2', '', '', '2019-10-03 04:14:23', '2019-10-03 04:14:23', '', 0, 'http://test.local.com/?page_id=34', 0, 'page', '', 0),
(35, 1, '2019-10-03 04:14:23', '2019-10-03 04:14:23', '', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2019-10-03 04:14:23', '2019-10-03 04:14:23', '', 34, 'http://test.local.com/2019/10/03/34-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2019-10-03 04:15:00', '2019-10-03 04:15:00', '{\n    \"nav_menu_item[-8327095201501817000]\": {\n        \"value\": {\n            \"object_id\": 34,\n            \"object\": \"page\",\n            \"menu_item_parent\": 0,\n            \"position\": 3,\n            \"type\": \"post_type\",\n            \"title\": \"Privacy Policy\",\n            \"url\": \"http://test.local.com/privacy-policy-2/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Privacy Policy\",\n            \"nav_menu_term_id\": 18,\n            \"_invalid\": false,\n            \"type_label\": \"Page\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:15:00\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '0f8c903f-7a6a-45f7-8374-f1317453bd34', '', '', '2019-10-03 04:15:00', '2019-10-03 04:15:00', '', 0, 'http://test.local.com/2019/10/03/0f8c903f-7a6a-45f7-8374-f1317453bd34/', 0, 'customize_changeset', '', 0),
(37, 1, '2019-10-03 04:15:00', '2019-10-03 04:15:00', ' ', '', '', 'publish', 'closed', 'closed', '', '37', '', '', '2019-10-03 04:15:00', '2019-10-03 04:15:00', '', 0, 'http://test.local.com/2019/10/03/37/', 3, 'nav_menu_item', '', 0),
(38, 1, '2019-10-03 04:25:58', '0000-00-00 00:00:00', '{\n    \"sidebars_widgets[footer-area-3]\": {\n        \"value\": [\n            \"custom_html-3\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:23:50\"\n    },\n    \"widget_custom_html[3]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YToyOntzOjU6InRpdGxlIjtzOjEwOiJORVdTTEVUVEVSIjtzOjc6ImNvbnRlbnQiO3M6NDM6Iltjb250YWN0LWZvcm0tNyBpZD0iMzkiIHRpdGxlPSJORVdTTEVUVEVSIl0iO30=\",\n            \"title\": \"NEWSLETTER\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"2deb0b13549a30ae4ecbcb4e89b0f9a4\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:25:58\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'dbfc47ff-bccf-4b76-bc9d-944416361e3a', '', '', '2019-10-03 04:25:58', '2019-10-03 04:25:58', '', 0, 'http://test.local.com/?p=38', 0, 'customize_changeset', '', 0),
(40, 1, '2019-10-03 04:30:11', '2019-10-03 04:30:11', '[text* first_name placeholder \"First Name\"]\r\n[text* last_name placeholder \"Last Name\"]\r\n[email* email placeholder \"Email Address\"]\r\n[submit \"Sign up\"]\n1\nSALIENT \"[your-subject]\"\nSALIENT <wordpress@test.local.com>\nrusiru@leafcutter.com.au\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis email was sent from a contact form on SALIENT (http://test.local.com)\nReply-To: [your-email]\n\n\n\n\nSALIENT \"[your-subject]\"\nSALIENT <wordpress@test.local.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis email was sent from a contact form on SALIENT (http://test.local.com)\nReply-To: rusiru@leafcutter.com.au\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe email address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'NEWSLETTER', '', 'publish', 'closed', 'closed', '', 'contact-form-1_copy', '', '', '2019-10-03 06:33:01', '2019-10-03 06:33:01', '', 0, 'http://test.local.com/?post_type=wpcf7_contact_form&#038;p=40', 0, 'wpcf7_contact_form', '', 0),
(41, 1, '2019-10-03 04:31:14', '2019-10-03 04:31:14', '{\n    \"sidebars_widgets[footer-area-3]\": {\n        \"value\": [\n            \"custom_html-3\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:31:14\"\n    },\n    \"widget_custom_html[3]\": {\n        \"value\": {\n            \"encoded_serialized_instance\": \"YToyOntzOjU6InRpdGxlIjtzOjEwOiJORVdTTEVUVEVSIjtzOjc6ImNvbnRlbnQiO3M6NTI6Iltjb250YWN0LWZvcm0tNyBpZD0iNDAiIHRpdGxlPSJDb250YWN0IGZvcm0gMV9jb3B5Il0iO30=\",\n            \"title\": \"NEWSLETTER\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"b1e6015d001efa9c220853631d4f4bab\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-03 04:31:14\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'f4ea7f5e-04c2-4783-a21a-d43fcffa2f99', '', '', '2019-10-03 04:31:14', '2019-10-03 04:31:14', '', 0, 'http://test.local.com/2019/10/03/f4ea7f5e-04c2-4783-a21a-d43fcffa2f99/', 0, 'customize_changeset', '', 0),
(42, 1, '2019-10-03 05:01:22', '2019-10-03 05:01:22', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector</p>\n<p>[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector</p>\n<p>[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 05:01:22', '2019-10-03 05:01:22', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2019-10-03 05:04:11', '2019-10-03 05:04:11', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"90\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector</p>\n<p>[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"90\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector</p>\n<p>[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 05:04:11', '2019-10-03 05:04:11', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2019-10-03 05:05:05', '2019-10-03 05:05:05', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector</p>\n<p>[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector</p>\n<p>[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 05:05:05', '2019-10-03 05:05:05', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(45, 1, '2019-10-03 05:11:14', '2019-10-03 05:11:14', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]Movies[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector</p>\n<p>[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector</p>\n<p>[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 05:11:14', '2019-10-03 05:11:14', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0),
(46, 0, '2019-10-03 05:19:12', '2019-10-03 05:19:12', '[]', 'woocommerce_update_marketplace_suggestions', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d9584e53d5354.67705017-Q13QO9MhkYyzo9XW6wz8XFdZH97UsXXl', '', '', '2019-10-03 05:19:33', '2019-10-03 05:19:33', '', 0, 'http://test.local.com/?post_type=scheduled-action&#038;p=46', 0, 'scheduled-action', '', 3),
(47, 1, '2019-10-03 05:20:13', '0000-00-00 00:00:00', '', 'AUTO-DRAFT', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2019-10-03 05:20:13', '0000-00-00 00:00:00', '', 0, 'http://test.local.com/?post_type=product&p=47', 0, 'product', '', 0),
(48, 1, '2019-10-03 05:51:12', '2019-10-03 05:51:12', '', 'Movie 1', '', 'publish', 'open', 'closed', '', 'movie-1', '', '', '2019-10-04 08:52:30', '2019-10-04 08:52:30', '', 0, 'http://test.local.com/?post_type=portfolio&#038;p=48', 0, 'portfolio', '', 0),
(49, 1, '2019-10-03 05:51:12', '2019-10-03 05:51:12', '', 'Movie 1', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2019-10-03 05:51:12', '2019-10-03 05:51:12', '', 48, 'http://test.local.com/2019/10/03/48-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2019-10-03 05:59:22', '2019-10-03 05:59:22', '', '1550457733', '', 'inherit', 'open', 'closed', '', '1550457733', '', '', '2019-10-03 05:59:22', '2019-10-03 05:59:22', '', 48, 'http://test.local.com/wp-content/uploads/2019/10/1550457733.gif', 0, 'attachment', 'image/gif', 0),
(51, 1, '2019-10-03 06:01:16', '2019-10-03 06:01:16', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]Movies[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"4\" category=\"all\" project_style=\"1\" item_spacing=\"default\" load_in_animation=\"none\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 06:01:16', '2019-10-03 06:01:16', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2019-10-03 06:02:06', '2019-10-03 06:02:06', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]Movies[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"4\" category=\"all\" project_style=\"1\" item_spacing=\"default\" load_in_animation=\"none\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 06:02:06', '2019-10-03 06:02:06', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2019-10-03 06:02:55', '2019-10-03 06:02:55', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]Movies[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"4\" category=\"all\" project_style=\"1\" item_spacing=\"default\" enable_pagination=\"true\" pagination_type=\"default\" load_in_animation=\"none\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 06:02:55', '2019-10-03 06:02:55', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2019-10-03 06:07:42', '2019-10-03 06:07:42', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]Movies[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"4\" category=\"all\" starting_category=\"default\" project_style=\"1\" item_spacing=\"default\" masonry_style=\"true\" bypass_image_cropping=\"true\" enable_sortable=\"true\" filter_color=\"default\" enable_pagination=\"true\" pagination_type=\"infinite_scroll\" load_in_animation=\"none\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 06:07:42', '2019-10-03 06:07:42', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2019-10-03 06:12:15', '2019-10-03 06:12:15', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]Movies[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"4\" category=\"all\" starting_category=\"default\" project_style=\"1\" item_spacing=\"default\" enable_sortable=\"true\" horizontal_filters=\"true\" filter_alignment=\"center\" filter_color=\"default\" enable_pagination=\"true\" pagination_type=\"infinite_scroll\" load_in_animation=\"fade_in_from_bottom\" projects_per_page=\"10\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 06:12:15', '2019-10-03 06:12:15', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2019-10-03 06:15:18', '2019-10-03 06:15:18', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]Movies[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"4\" category=\"all\" starting_category=\"default\" project_style=\"1\" item_spacing=\"default\" enable_sortable=\"true\" horizontal_filters=\"true\" filter_alignment=\"center\" filter_color=\"extra-color-3\" enable_pagination=\"true\" pagination_type=\"infinite_scroll\" load_in_animation=\"fade_in_from_bottom\" projects_per_page=\"10\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 06:15:18', '2019-10-03 06:15:18', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2019-10-03 06:18:53', '2019-10-03 06:18:53', '', 'Movie 2', '', 'publish', 'open', 'closed', '', 'movie-2', '', '', '2019-10-03 06:20:09', '2019-10-03 06:20:09', '', 0, 'http://test.local.com/?post_type=portfolio&#038;p=57', 0, 'portfolio', '', 0),
(58, 1, '2019-10-03 06:18:53', '2019-10-03 06:18:53', '', 'Movie 2', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2019-10-03 06:18:53', '2019-10-03 06:18:53', '', 57, 'http://test.local.com/2019/10/03/57-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2019-10-03 06:19:31', '2019-10-03 06:19:31', '', 'Movie 3', '', 'publish', 'open', 'closed', '', 'movie-3', '', '', '2019-10-03 06:19:45', '2019-10-03 06:19:45', '', 0, 'http://test.local.com/?post_type=portfolio&#038;p=59', 0, 'portfolio', '', 0),
(60, 1, '2019-10-03 06:19:31', '2019-10-03 06:19:31', '', 'Movie 3', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2019-10-03 06:19:31', '2019-10-03 06:19:31', '', 59, 'http://test.local.com/2019/10/03/59-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2019-10-03 06:21:14', '2019-10-03 06:21:14', '', 'Movie 4', '', 'publish', 'open', 'closed', '', 'movie-4', '', '', '2019-10-03 06:21:14', '2019-10-03 06:21:14', '', 0, 'http://test.local.com/?post_type=portfolio&#038;p=61', 0, 'portfolio', '', 0),
(62, 1, '2019-10-03 06:21:14', '2019-10-03 06:21:14', '', 'Movie 4', '', 'inherit', 'closed', 'closed', '', '61-revision-v1', '', '', '2019-10-03 06:21:14', '2019-10-03 06:21:14', '', 61, 'http://test.local.com/2019/10/03/61-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2019-10-03 06:22:19', '2019-10-03 06:22:19', '', 'Movie 5', '', 'publish', 'open', 'closed', '', 'movie-5', '', '', '2019-10-03 06:23:21', '2019-10-03 06:23:21', '', 0, 'http://test.local.com/?post_type=portfolio&#038;p=63', 0, 'portfolio', '', 0),
(64, 1, '2019-10-03 06:22:19', '2019-10-03 06:22:19', '', 'Movie 5', '', 'inherit', 'closed', 'closed', '', '63-revision-v1', '', '', '2019-10-03 06:22:19', '2019-10-03 06:22:19', '', 63, 'http://test.local.com/2019/10/03/63-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2019-10-03 06:22:51', '2019-10-03 06:22:51', '', 'Movie 6', '', 'publish', 'open', 'closed', '', 'movie-6', '', '', '2019-10-03 06:22:51', '2019-10-03 06:22:51', '', 0, 'http://test.local.com/?post_type=portfolio&#038;p=65', 0, 'portfolio', '', 1),
(66, 1, '2019-10-03 06:22:51', '2019-10-03 06:22:51', '', 'Movie 6', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2019-10-03 06:22:51', '2019-10-03 06:22:51', '', 65, 'http://test.local.com/2019/10/03/65-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2019-10-03 06:28:02', '2019-10-03 06:28:02', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]Movies[/heading][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"4\" category=\"all\" starting_category=\"default\" project_style=\"1\" item_spacing=\"default\" enable_sortable=\"true\" horizontal_filters=\"true\" filter_alignment=\"center\" filter_color=\"extra-color-3\" enable_pagination=\"true\" pagination_type=\"infinite_scroll\" load_in_animation=\"fade_in_from_bottom\" projects_per_page=\"10\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 06:28:02', '2019-10-03 06:28:02', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(68, 1, '2019-10-03 06:29:37', '2019-10-03 06:29:37', '<p>[vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" equal_height=\"yes\" content_placement=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"right\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_slider location=\"Slider1\" arrow_navigation=\"true\" overall_style=\"classic\" bullet_navigation=\"true\" bg_animation=\"none\" slider_transition=\"slide\" caption_transition=\"fade_in_from_bottom\" button_sizing=\"regular\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_icon icon_family=\"fontawesome\" icon_style=\"default\" icon_color=\"Extra-Color-2\" icon_padding=\"20px\" icon_fontawesome=\"fa fa-user-circle\" margin_left=\"100\"][vc_column_text]</p>\n<p style=\"text-align: center;\">Person Name<br />\nDirector[/vc_column_text][/vc_column][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/4\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]Movies[/heading][/vc_column][/vc_row][vc_row type=\"full_width_content\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column centered_text=\"true\" column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][nectar_portfolio layout=\"fullwidth\" category=\"all\" starting_category=\"default\" project_style=\"1\" item_spacing=\"default\" enable_sortable=\"true\" horizontal_filters=\"true\" filter_alignment=\"center\" filter_color=\"extra-color-3\" enable_pagination=\"true\" pagination_type=\"infinite_scroll\" load_in_animation=\"fade_in_from_bottom\" projects_per_page=\"10\"][/vc_column][/vc_row]</p>\n', 'Home', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-10-03 06:29:37', '2019-10-03 06:29:37', '', 7, 'http://test.local.com/2019/10/03/7-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2019-10-03 08:09:11', '2019-10-03 08:09:11', '<p>[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#aaaaaa\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]</p>\n<h1>Contact Us</h1>\n<p>[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]</p>\n<h2 style=\"text-align: center;\"><strong>Get in Touch </strong></h2>\n<h5 style=\"text-align: center;\">We would like to hear you feedback.</h5>\n<p>[/vc_column_text][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_raw_html]JTVCY29udGFjdC1mb3JtLTclMjBpZCUzRCUyMjYlMjIlMjB0aXRsZSUzRCUyMkNvbnRhY3QlMjBmb3JtJTIwMSUyMiU1RA==[/vc_raw_html][/vc_column][/vc_row]</p>\n', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2019-10-03 08:09:11', '2019-10-03 08:09:11', '', 20, 'http://test.local.com/2019/10/03/20-revision-v1/', 0, 'revision', '', 0),
(70, 1, '2019-10-03 08:10:36', '2019-10-03 08:10:36', '<p>[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#969696\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]</p>\n<h1>Contact Us</h1>\n<p>[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]</p>\n<h2 style=\"text-align: center;\"><strong>Get in Touch </strong></h2>\n<h5 style=\"text-align: center;\">We would like to hear you feedback.</h5>\n<p>[/vc_column_text][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_raw_html]JTVCY29udGFjdC1mb3JtLTclMjBpZCUzRCUyMjYlMjIlMjB0aXRsZSUzRCUyMkNvbnRhY3QlMjBmb3JtJTIwMSUyMiU1RA==[/vc_raw_html][/vc_column][/vc_row]</p>\n', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2019-10-03 08:10:36', '2019-10-03 08:10:36', '', 20, 'http://test.local.com/2019/10/03/20-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2019-10-03 08:11:08', '2019-10-03 08:11:08', '<p>[vc_row type=\"full_width_background\" full_screen_row_position=\"middle\" bg_color=\"#969696\" scene_position=\"center\" text_color=\"light\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\" shape_type=\"\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][heading]</p>\n<h1>Contact Us</h1>\n<p>[/heading][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_column_text]</p>\n<h2 style=\"text-align: center;\"><strong>Get in Touch </strong></h2>\n<p style=\"text-align: center;\">We would like to hear you feedback.</p>\n<p>[/vc_column_text][/vc_column][/vc_row][vc_row type=\"in_container\" full_screen_row_position=\"middle\" scene_position=\"center\" text_color=\"dark\" text_align=\"left\" overlay_strength=\"0.3\" shape_divider_position=\"bottom\" bg_image_animation=\"none\"][vc_column column_padding=\"no-extra-padding\" column_padding_position=\"all\" background_color_opacity=\"1\" background_hover_color_opacity=\"1\" column_link_target=\"_self\" column_shadow=\"none\" column_border_radius=\"none\" width=\"1/1\" tablet_width_inherit=\"default\" tablet_text_alignment=\"default\" phone_text_alignment=\"default\" column_border_width=\"none\" column_border_style=\"solid\" bg_image_animation=\"none\"][vc_raw_html]JTVCY29udGFjdC1mb3JtLTclMjBpZCUzRCUyMjYlMjIlMjB0aXRsZSUzRCUyMkNvbnRhY3QlMjBmb3JtJTIwMSUyMiU1RA==[/vc_raw_html][/vc_column][/vc_row]</p>\n', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2019-10-03 08:11:08', '2019-10-03 08:11:08', '', 20, 'http://test.local.com/2019/10/03/20-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2019-10-03 10:52:00', '2019-10-03 10:52:00', '<!-- wp:paragraph -->\r\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\r\n<!-- /wp:paragraph -->', 'Hello world 2', '', 'publish', 'open', 'open', '', 'hello-world-2', '', '', '2019-10-03 10:52:00', '2019-10-03 10:52:00', '', 0, 'http://test.local.com/?p=72', 0, 'post', '', 0),
(73, 1, '2019-10-03 10:52:00', '2019-10-03 10:52:00', '<!-- wp:paragraph -->\r\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\r\n<!-- /wp:paragraph -->', 'Hello world 2', '', 'inherit', 'closed', 'closed', '', '72-revision-v1', '', '', '2019-10-03 10:52:00', '2019-10-03 10:52:00', '', 72, 'http://test.local.com/2019/10/03/72-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 19, 'order', '0'),
(2, 19, 'display_type', ''),
(3, 19, 'thumbnail_id', '0'),
(4, 20, 'order', '0'),
(5, 20, 'display_type', ''),
(6, 20, 'thumbnail_id', '0'),
(7, 21, 'order', '0'),
(8, 21, 'display_type', ''),
(9, 21, 'thumbnail_id', '0'),
(10, 22, 'order', '0'),
(11, 22, 'display_type', ''),
(12, 22, 'thumbnail_id', '0'),
(13, 23, 'order', '0'),
(14, 23, 'display_type', ''),
(15, 23, 'thumbnail_id', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorised', 'uncategorised', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0),
(16, 'Slider1', 'slider1', 0),
(17, 'main_menu', 'main_menu', 0),
(18, 'Quick_Link_Menu', 'quick_link_menu', 0),
(19, 'Action', 'action', 0),
(20, 'Drama', 'drama', 0),
(21, 'Horror', 'horror', 0),
(22, 'Romance', 'romance', 0),
(23, 'Sci-fi', 'sci-fi', 0),
(24, 'Action', 'action', 0),
(25, 'Dreama', 'dreama', 0),
(26, 'Horror', 'horror', 0),
(27, 'Romance', 'romance', 0),
(28, 'Sci-fi', 'sci-fi', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(12, 16, 0),
(18, 16, 0),
(22, 17, 0),
(23, 17, 0),
(30, 18, 0),
(31, 18, 0),
(37, 18, 0),
(48, 24, 0),
(57, 25, 0),
(59, 25, 0),
(61, 26, 0),
(63, 27, 0),
(65, 28, 0),
(72, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2),
(2, 2, 'product_type', '', 0, 0),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(16, 16, 'slider-locations', '', 0, 2),
(17, 17, 'nav_menu', '', 0, 2),
(18, 18, 'nav_menu', '', 0, 3),
(19, 19, 'product_cat', '', 0, 0),
(20, 20, 'product_cat', '', 0, 0),
(21, 21, 'product_cat', '', 0, 0),
(22, 22, 'product_cat', '', 0, 0),
(23, 23, 'product_cat', '', 0, 0),
(24, 24, 'project-type', '', 0, 1),
(25, 25, 'project-type', '', 0, 2),
(26, 26, 'project-type', '', 0, 1),
(27, 27, 'project-type', '', 0, 1),
(28, 28, 'project-type', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'ruzi'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'vc_pointers_backend_editor,vc_pointers_frontend_editor,theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:4:{s:64:\"f196aa495ccf38177151127ad3b53d6f446b8670a5c017ca0aa83b010967d4ae\";a:4:{s:10:\"expiration\";i:1570177489;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1570004689;}s:64:\"455a08e4a52aedb40fb9f31112ab90c550d280050513ff6f862fa5e1f947e836\";a:4:{s:10:\"expiration\";i:1570177759;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1570004959;}s:64:\"87c2ba4078d57aa9e815e9ed9d53eb220fe1d9fd6535ed90d95bce891e4665f9\";a:4:{s:10:\"expiration\";i:1570182962;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1570010162;}s:64:\"c65952728dd7fefecf3d8c44b4c71a05d45cc283bf74e18575efbd34dc5185c1\";a:4:{s:10:\"expiration\";i:1570244266;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1570071466;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'wp_user-settings', 'edit_element_vcUIPanelWidth=651&edit_element_vcUIPanelLeft=427px&edit_element_vcUIPanelTop=84px&libraryContent=browse&editor=tinymce&post_settings_vcUIPanelWidth=651&post_settings_vcUIPanelLeft=872px&post_settings_vcUIPanelTop=74px&post_dfw=off'),
(20, 1, 'wp_user-settings-time', '1570175481'),
(21, 1, '_woocommerce_tracks_anon_id', 'woo:qTJBXWdo5Gn2O0HiQLIIEgTU'),
(22, 1, 'wc_last_active', '1570147200'),
(23, 1, 'wp_r_tru_u_x', 'a:2:{s:2:\"id\";s:0:\"\";s:7:\"expires\";i:86400;}'),
(25, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(26, 1, 'metaboxhidden_nav-menus', 'a:9:{i:0;s:23:\"add-post-type-portfolio\";i:1;s:21:\"add-post-type-product\";i:2;s:25:\"add-post-type-home_slider\";i:3;s:12:\"add-post_tag\";i:4;s:15:\"add-post_format\";i:5;s:16:\"add-project-type\";i:6;s:22:\"add-project-attributes\";i:7;s:15:\"add-product_cat\";i:8;s:15:\"add-product_tag\";}'),
(27, 1, 'nav_menu_recently_edited', '18'),
(28, 1, 'closedpostboxes_portfolio', 'a:4:{i:0;s:19:\"wpb_visual_composer\";i:1;s:36:\"nectar-metabox-project-configuration\";i:2;s:26:\"nectar-metabox-page-header\";i:3;s:30:\"nectar-metabox-portfolio-video\";}'),
(29, 1, 'metaboxhidden_portfolio', 'a:1:{i:0;s:7:\"slugdiv\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'ruzi', '$P$BHWmSfGiPBHYatp4FaYCSW7AGwxQ4j0', 'ruzi', 'rusiru@leafcutter.com.au', '', '2019-10-02 08:24:36', '', 0, 'ruzi');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_download_log`
--

CREATE TABLE `wp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_product_meta_lookup`
--

CREATE TABLE `wp_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(10,2) DEFAULT NULL,
  `max_price` decimal(10,2) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_tax_rate_classes`
--

CREATE TABLE `wp_wc_tax_rate_classes` (
  `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_wc_tax_rate_classes`
--

INSERT INTO `wp_wc_tax_rate_classes` (`tax_rate_class_id`, `name`, `slug`) VALUES
(1, 'Reduced rate', 'reduced-rate'),
(2, 'Zero rate', 'zero-rate');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_webhooks`
--

CREATE TABLE `wp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_log`
--

CREATE TABLE `wp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_sessions`
--

INSERT INTO `wp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(1, '1', 'a:7:{s:4:\"cart\";s:6:\"a:0:{}\";s:11:\"cart_totals\";s:367:\"a:15:{s:8:\"subtotal\";i:0;s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";i:0;s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";i:0;s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";i:0;s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";i:0;s:9:\"total_tax\";i:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:8:\"customer\";s:712:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"GB\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"GB\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:24:\"rusiru@leafcutter.com.au\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1570184906);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_cf7_vdata`
--
ALTER TABLE `wp_cf7_vdata`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_cf7_vdata_entry`
--
ALTER TABLE `wp_cf7_vdata_entry`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `wp_wc_product_meta_lookup`
--
ALTER TABLE `wp_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `wp_wc_tax_rate_classes`
--
ALTER TABLE `wp_wc_tax_rate_classes`
  ADD PRIMARY KEY (`tax_rate_class_id`),
  ADD UNIQUE KEY `slug` (`slug`(191));

--
-- Indexes for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_cf7_vdata`
--
ALTER TABLE `wp_cf7_vdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_cf7_vdata_entry`
--
ALTER TABLE `wp_cf7_vdata_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1008;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=644;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wc_tax_rate_classes`
--
ALTER TABLE `wp_wc_tax_rate_classes`
  MODIFY `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD CONSTRAINT `fk_wp_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wp_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
